

<!DOCTYPE html>
<%@ page import="org.owasp.encoder.Encoder" %>
<html>
<head>
  <title></title>
  <header><h1>Contáctame</h1></header>
  <link rel="stylesheet" type="text/css" href="style.css">
  <meta charset="utf-8">
</head>

<body>
  <%= Encode.forHtml(text) %>
  <h3>¿Por qué es una buena práctica separar el controlador de la vista?</h3>
  <p>Para tener buenas prácticas a la hora de hacer código, permite separar los componentes y por lo tantyo, hacer más mantenible el código</p>
  <h3>Aparte de los arreglos $_POST y $_GET, ¿qué otros arreglos están predefinidos en php y cuál es su función?</h3>
  <p>
    $_COOKIE:Variable tipo array asociativo de variables pasadas al script, a través de Cookies HTTP.
    $_REQUEST: se proporcionan al script a través de  GET, POST, y COOKIE y pueden ser manipulados
  </p>
  <h3>Explora las funciones de php, y describe 2 que no hayas visto en otro lenguaje y que llamen tu atención.</h3>
  <p>
    isset(): se asegura de que la variable esté definida
    preg_match(): buscar o comparar cadenas mediante expresiones regulares
  </p> 
  <h3>¿Qué es XSS y cómo se puede prevenir?</h3>
  <p>Se refiere a la vulnerabilidad informática que además es bastante común en los sitios web mediante: seceuncias de comandos maliciosas e instalar malware en los navegadores del usuario del sitio web.
  Se pueden evitar utilizando frameworks seguros, codificar los requerimientos de HTTP, aplicar codificación sensitiva al contexto o habilitar politicas de seguridad.</p>
<div class="nvo">
  <%-- HTML Attribute context
<form name="contactform" method="post" action="send_form_email.php" id="al">
<table width="450px">
<tr>
 <td valign="top">
  <label for="first_name">Nombre</label>
 </td>
 <td valign="top">
  <input  type="text" name="first_name" maxlength="50" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="last_name">Apellidos</label>
 </td>
 <td valign="top">
  <input  type="text" name="last_name" maxlength="50" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="email">Email</label>
 </td>
 <td valign="top">
  <input  type="text" name="email" maxlength="80" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="telephone">Teléfono</label>
 </td>
 <td valign="top">
  <input  type="text" name="telephone" maxlength="30" size="30">
 </td>
</tr>
<tr>
 <td valign="top">
  <label for="message">Mensaje</label>
 </td>
 <td valign="top">
  <textarea  name="message" maxlength="1000" cols="25" rows="6"></textarea>
 </td>
</tr>
<tr>
 <td colspan="2" >
  <input type="submit" value="Enviar">   <a href="http://www.tufelicidadvacacional.com.ve/email_form.php"></a>
 </td>
</tr>
</table>
</form>
</div>
</body>
</html>






