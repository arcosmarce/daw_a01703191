-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-03-2020 a las 22:01:26
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyeectos`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `COL 1` int(4) DEFAULT NULL,
  `COL 2` varchar(37) DEFAULT NULL,
  `COL 3` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`COL 1`, `COL 2`, `COL 3`) VALUES
(5000, 'Vamos Mexico', NULL),
(5001, 'Aztec?n\n5002', 'CIT Campeche'),
(5003, 'Mexico sin ti no estamos completos', NULL),
(5004, 'Educando en Coahuila', NULL),
(5005, 'Infonavit Durango', NULL),
(5006, 'Reconstrucci?n del templo de Guadalup', NULL),
(5007, 'Construcci?n de plaza Magnolias', NULL),
(5008, 'Televisa en acci?n\n5009', 'Disco Atlantic'),
(5010, 'Construcci?n de Hospital Infantil', NULL),
(5011, 'Remodelaci?n de aulas del IPP', NULL),
(5012, 'Restauraci?n de instalaciones del CEA', NULL),
(5013, 'Reparaci?n de la plaza Sonora', NULL),
(5014, 'Remodelaci?n de Soriana', NULL),
(5015, 'CIT Yucatan', NULL),
(5016, 'Ampliaci?n de la carretera a la huast', NULL),
(5017, 'Reparaci?n de la carretera del sol', NULL),
(5018, 'Tu cambio por la educacion', NULL),
(5019, 'Queretaro limpio', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
