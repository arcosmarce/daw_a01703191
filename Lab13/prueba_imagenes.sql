

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `prueba_imagenes` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `prueba_imagenes`;

CREATE TABLE IF NOT EXISTS `imagenes` (
  `imagen_id` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO `imagenes` (`imagen_id`, `imagen`) VALUES
(10, 'avance_1.jpg'),
(11, 'Chrysantemum.jpg');

ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`imagen_id`);

ALTER TABLE `imagenes`
  MODIFY `imagen_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
