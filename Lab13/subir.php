<?php
require('conexion.php');
$con = new conexion();

if ($_FILES['imagen']['error']>0) {
	echo "ha ocurrido un error en la subida";
} else{
	$permitidos =array("image/jpg","image/jpeg","image/png","image/gif");
	$limite_kb = 1000;
	if (in_array($_FILES['imagen']['type'], $permitidos) && $_FILES['imagen']['size'] <= $limite_kb * 1024) {
		$ruta="imagenes/".$_FILES['imagen']['name'];
		if (!file_exists($ruta)) {
			$resultado = @move_uploaded_file($_FILES["imagen"]["tmp_name"], $ruta);
			if ($resultado) {
				$nombre = $_FILES['imagen']['name'];
					if ($con->subir_imagen($nombre)) {
					echo "el archivo ha sido subido exitosamente<br>";
					} else {
					die("error");
					}
			} else{
				echo "ocurrio un error al mover el archivo";
			}
		} else{
			echo $_FILES['imagen']['name'].", este archivo existe ";
			echo "<br>".$_FILES['imagen']['size'];
		}
	} else {
		echo "Archivo no permitido, este tipo de archivo es prohibido o excede el tamanio de ".$limite_kb." Kilobytes <br>";
	}
}

?>