<?php include 'component_admin_navbar.php'; ?>


<main>
	<div class="container">


		<h5 class="text-center m-4">Proyectos</h5>

		<div class="container d-flex flex-column justify-content-center align-items-center">
			<form class="flex-item w-25">
				<label for="periodo">
					<h5 class="text-center m-4">Periodo</h5>
				</label>
				<div class="container d-flex justify-content-around align-items-center border-bottom">
					<div class="flex-item m-2">
						<select class="form-control s" ng-model="periodo_seleccionado" ng-options="periodo as periodo.periodo_nombre for periodo in periodos" id="periodo"></select>
					</div>
					<div class="flex-item m-2">
						<button type="button" class="btn btn-blue m-2" ng-click="consultar_proyectos()">Consultar</button>
					</div>
				</div>
			</form>
		</div>

		<table class="table table-light">
			<thead class="bg-blue-pastel text-dark ">
				<tr>
					<th scope="col">Nombre</th>
					<th scope="col">Vacantes</th>
					<th scope="col">Horas de acreditación</th>
					<th scope="col">Estatus</th>
					<th scope="col">Acciones</th>
				</tr>
			</thead>

			<tr>
				<td><input size="8" class="rounded" ng-model="f.proyecto_nombre"></td>
				<td><input size="8" class="rounded" ng-model="f.proyecto_vacantes"></td>
				<td><input size="8" class="rounded" ng-model="f.proyecto_horas_acreditar"></td>
				<td><input size="8" class="rounded" ng-model="f.proyecto_estatus"></td>
			</tr>




			<tbody ng-repeat="x in proyectos | filter:f">

				<tr>


					<th scope="row">{{x.proyecto_nombre}}</th>
					<td>{{x.proyecto_vacantes}}</td>
					<td>{{x.proyecto_horas_acreditar}}</td>
					<td>{{x.proyecto_estatus}}</td>
					<!--Botones de acciones -->

					<td>

						<button class="btn btn-orange-pastel btn-circle btn-sm p-2 m-1" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar">
							<i class="fas fa-trash">
							</i>
						</button>
						<button class="btn btn-green-pastel btn-circle btn-sm p-2 m-1" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Archivar">
							<i class="fas fa-archive">
							</i>
						</button>

						<button type="button" class="btn btn-circle btn-sm p-2 m-1" data-toggle="collapse" data-target="#detalles{{x.proyecto_id}}" data-toggle="tooltip" data-placement="top" title="Detalles" aria-expanded="false" aria-controls="detalles{{x.proyecto_id}}">
							<i class="fas fa-chevron-down js-rotate-if-collapsed">
							</i>
						</button>

					</td>
					<!--End Botones de acciones-->
				</tr>




				<!--Detalles-->
				<tr class="collapse" id="detalles{{x.proyecto_id}}">
					<td colspan="5">
						<div class="container">

							<div class="row">


								<div class="col-sm">

									<div class="card w-100" style="width: 18rem;">



										<div class="card-body">



											<h5 class="card-title">Descripción</h5>
											<p class="card-text">

												<div class="container">

													<div class="row">
														<div class="col-sm">
															<h6 class="text-left m-1"> Duración</h6>
														</div>
														<div class="col-sm">
															{{x.proyecto_duracion}}
														</div>
													</div>

													<div class="row">
														<div class="col-sm">
															<h6 class="text-left m-1"> Lugar</h6>
														</div>
														<div class="col-sm">
															{{x.proyecto_lugar}}
														</div>
													</div>

													<div class="row">
														<div class="col-sm">
															<h6 class="text-left m-1"> Horario</h6>
														</div>
														<div class="col-sm">
															{{x.proyecto_horario}}
														</div>
													</div>

													<div class="row">
														<div class="col-sm">
															<h6 class="text-center m-1"> Objetivo</h6>
														</div>
													</div>

													<div class="row">
														<div class="col-sm">
															<p class="text-justify">
																{{x.proyecto_objetivo}}
															</p>

														</div>
													</div>
													<div ng-include="'components/partials/_proyectos_actividades.html'"></div>
												</div>
										</div>
										</p>
									</div>
								</div>


								<div class="col-sm">
									<div class="card w-100" style="width: 18rem;">

										<div class="card-body">
											<h5 class="card-title">Perfil</h5>

										</div>
									</div>
								</div>

								<div class="col-sm">
									<div class="card w-100" style="width: 18rem;">

										<div class="card-body">
											<h5 class="card-title">Contacto</h5>

										</div>
									</div>
								</div>


							</div>


						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</main>