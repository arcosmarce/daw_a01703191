<html ng-app="myApp">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- CSS -->
	<!-- Main css is a personalized version of Bootsrap, it can be customized using the main.scss file, located in resources/css -->
	<!-- To compile, write in the terminal: sass main.scss main.css -->
	<link href="resources/css/navbar.css" rel="stylesheet">
	<link href="resources/css/buttons.css" rel="stylesheet">
	<link href="resources/css/collapse_icon.css" rel="stylesheet">
	<link href="resources/css/pdfviewer.css" rel="stylesheet">
	<link href="resources/css/evaluacion.css" rel="stylesheet">
	<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css" rel="stylesheet">
	<link href="resources/css/main.css" rel="stylesheet">
	<link href="resources/css/angular.css" rel="stylesheet">

	<!-- Google Material Icons and font -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700&display=swap" rel="stylesheet" />
	<!-- Font Awesome Icons -->
	<script src="https://kit.fontawesome.com/d7dae8da40.js" crossorigin="anonymous"></script>
	<!-- Angular -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-route.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular-animate.js"></script>


	<!--routes -->
	<script src="./mvc/controllers/routes.js"></script>
	<!--services -->
	<script src="./mvc/controllers/services/service_session.js"></script>
	<!--controllers -->
	<script src="./mvc/controllers/controller_login.js"></script>
	<script src="./mvc/controllers/controller_navbar.js"></script>
	<script src="./mvc/controllers/controller_home.js"></script>
	<script src="./mvc/controllers/controller_proyectos.js"></script>
	<script src="./mvc/controllers/controller_convenio.js"></script>
	<script src="./mvc/controllers/controller_evaluacion.js"></script>
	<script src="./mvc/controllers/controller_acreditacion.js"></script>
	<script src="./mvc/controllers/controller_carta.js"></script>


	<style>
		input {
			text-align: center;
		}

		select {
			text-align: center;
		}

		select.select {
			width: auto;
			display: block;
			margin: 0 auto;
		}
	</style>
	<title><?php echo basename(__DIR__); ?></title>
</head>

<body>
	<div ng-view></div>
	<script src="resources/js/jquery-3-2-1-slim-min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="https://mozilla.github.io/pdf.js/build/pdf.js"></script>
	<!--<script src="resources/js/render.js"></script>
	<script src="resources/js/DataTable.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>-->
</body>

</html>