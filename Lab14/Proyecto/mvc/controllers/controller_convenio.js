app.controller('controller_convenio', function ($scope, $http, user) {
  $scope.user = user.getName();
  $scope.correo = user.getCorreo();
  $scope.rol = user.getRol();
  $scope.privilegios = user.getPrivilegios();
  $scope.rol_primary = user.getRolPrimary();
  $scope.rol_secondary = user.getRolSecondary();
  $scope.nav_rol = user.getNavRol();
  $scope.convenio_consult = false;
  $scope.convenio_upload = false;
  $scope.convenio_plantilla_upload = false;

  for (privilegio in $scope.privilegios) {
    switch ($scope.privilegios[privilegio]) {
      case "convenio_consult":
        $scope.convenio_consult = true;
        console.log("convenio_consult");
        break;
      case "convenio_upload":
        $scope.convenio_upload = true;
        console.log($scope.convenio_upload);
        break;
      case "convenio_plantilla_upload":
        $scope.convenio_plantilla_upload = true;
        console.log("convenio_plantilla_upload");
        break;
      default:
        break;
    }
  }



  $scope.tab_convenio_consult = true;
  $scope.tab_convenio_upload = false;
  $scope.tab_convenio_plantilla_upload = false;
  $scope.isAdmin = false;
  $scope.isSocio = false;
  switch ($scope.rol) {
    //admin
    case "1":
      $scope.isAdmin = true;
      $scope.isSocio = false;
      break;
      //socio
    case "2":
      $scope.isAdmin = false;
      $scope.isSocio = true;
      break;
    default:
      break;
  }


  $scope.showConvenioConsult = function () {
    $scope.tab_convenio_consult = true;
    $scope.tab_convenio_upload = false;
    $scope.tab_convenio_plantilla_upload = false;
  }

  $scope.showConvenioUpload = function () {
    $scope.tab_convenio_consult = false;
    $scope.tab_convenio_upload = true;
    $scope.tab_convenio_plantilla_upload = false;
  }

  $scope.showConvenioPlantillaUpload = function () {
    $scope.tab_convenio_consult = false;
    $scope.tab_convenio_upload = false;
    $scope.tab_convenio_plantilla_upload = true;
    console.log("tab_convenio_plantilla_upload");
    console.log($scope.tab_convenio_plantilla_upload && $scope.convenio_plantilla_upload);
  }

  $http.get("mvc/models/proyectos/model_periodos_qry.php")
    .then(function (response) {
      $scope.periodos = response.data;
    });


});