app.controller('controller_login', function ($scope, $http, $location, user) {
    $scope.login = function () {
        var username = $scope.username;
        var password = $scope.password;
        $http({
            url: 'mvc/models/model_login.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + username + '&password=' + password
        }).then(function (response) {
            console.log(response.data.user);
            console.log(response.data.privilegios);
            if (response.data.status == 'loggedin') {
                user.saveData(response.data);
                $location.path('/home');
            } else {
                alert('invalid login');
            }
        })
    }
});