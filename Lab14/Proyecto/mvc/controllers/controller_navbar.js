app.controller('controller_navbar', function ($scope, user, $location, $http) {
    $scope.user = user.getName();
    $scope.correo = user.getCorreo();
    $scope.rol = user.getRol();
    $scope.rol_primary = user.getRolPrimary();
    $scope.rol_secondary = user.getRolSecondary();
    $scope.nav_rol = user.getNavRol();




    $scope.logout = function () {
        $http.post("mvc/models/model_session_terminate.php")
            .then(function (response) {
                console.log(response.data);
            });
        user.clearData();
        $location.path('/');
    }
});