app.controller('controller_proyectos', function ($scope, $http, user) {

  $scope.user = user.getName();
  $scope.correo = user.getCorreo();
  $scope.rol = user.getRol();
  $scope.privilegios = user.getPrivilegios();
  $scope.rol_primary = user.getRolPrimary();
  $scope.rol_secondary = user.getRolSecondary();
  $scope.nav_rol = user.getNavRol();
  console.log("User: " + $scope.user);
  console.log("Correo: " + $scope.correo);
  console.log("Rol id: " + $scope.rol);
  console.log("Privielgios:");
  console.log($scope.privilegios);
  $scope.proyecto_consult = false;
  $scope.proyecto_create = false;
  $scope.proyecto_delete = false;
  $scope.proyecto_update = false;
  $scope.proyecto_validate = false;
  for (privilegio in $scope.privilegios) {
    switch ($scope.privilegios[privilegio]) {
      case "proyecto_consult":
        $scope.proyecto_consult = true;
        console.log($scope.proyecto_consult);
        break;
      case "proyecto_create":
        $scope.proyecto_create = true;
        console.log($scope.proyecto_create);
        break;
      case "proyecto_delete":
        $scope.proyecto_delete = true;
        console.log($scope.proyecto_delete);
        break;
      case "proyecto_update":
        $scope.proyecto_update = true;
        console.log($scope.proyecto_update);
        break;
      case "proyecto_validate":
        $scope.proyecto_validate = true;
        console.log($scope.proyecto_validate);
        break;
      default:
        break;
    }
  }
  $scope.tab_proyecto_consult = false;
  $scope.tab_proyecto_create = false;
  $scope.tab_proyecto_validate = false;
  $scope.isAdmin = false;
  $scope.isSocio = false;
  switch ($scope.rol) {
    //admin
    case "1":
      $scope.tab_proyecto_consult = true;
      $scope.tab_proyecto_create = false;
      $scope.tab_proyecto_validate = false;
      $scope.isAdmin = true;
      $scope.isSocio = false;
      break;
      //socio
    case "2":
      $scope.tab_proyecto_consult = true;
      $scope.tab_proyecto_create = false;
      $scope.tab_proyecto_validate = false;
      $scope.isAdmin = false;
      $scope.isSocio = true;
      //consultar proyectos de la organizacion del socio
      $http.get("mvc/models/proyectos/model_proyectos_organizacion_socio_qry.php")
        .then(function (response) {
          $scope.proyectos = response.data;
          $scope.proyectos_backup = angular.copy($scope.proyectos);
          console.log(response.data);
        });
      break;
    default:
      break;
  }

  $scope.showProyectoConsult = function () {
    $scope.tab_proyecto_consult = true;
    $scope.tab_proyecto_create = false;
    $scope.tab_proyecto_validate = false;
  }

  $scope.showProyectoCreate = function () {
    $scope.tab_proyecto_consult = false;
    $scope.tab_proyecto_create = true;
    $scope.tab_proyecto_validate = false;
  }

  $scope.showProyectoValidate = function () {
    $scope.tab_proyecto_consult = false;
    $scope.tab_proyecto_create = false;
    $scope.tab_proyecto_validate = true;
  }

  $scope.getProyecto = function () {
    if ($scope.tab_proyecto_consult || $scope.tab_proyecto_validate) {
      return $scope.proyectos;
    }
    return $scope.proyecto_nuevo;
  };

  $scope.getDetallesID = function () {
    if ($scope.tab_proyecto_consult) {
      return "consult";
    }else if ($scope.tab_proyecto_validate){
      return "validate";
    }
    return "create";
  };


  $scope.proyectos = [];
  $scope.organizaciones = [];
  $scope.proyectos_organizaciones = [];
  $scope.proyectos_backup = [];
  $scope.proyecto_nuevo = [{
    proyecto_id: 1110,
    proyecto_nombre: "",
    proyecto_vacantes: 5,
    proyecto_horas_acreditar: "240",
    proyecto_objetivo: "",
    proyecto_perfil: "",
    proyecto_responsable_nombre: "",
    proyecto_responsable_puesto: "",
    proyecto_responsable_correo: "",
    proyecto_responsable_telefono: "",
    proyecto_disponible_whatsapp: "",
    proyecto_consideraciones: "",
    proyecto_estatus: "Disponible para renovar",
    proyecto_archivado: 1,
    proyecto_actividades: [],
    proyecto_horarios_trabajo: [],
    proyecto_lugares_servicio: [],
    proyecto_carreras: [],
    proyecto_estilos_trabajo: []
  }];
  $scope.actividad_nueva = [];
  $scope.carrera_nueva = [];
  $scope.estilo_trabajo_nuevo = [];
  $scope.horario_trabajo_nuevo = [];
  $scope.lugar_servicio_nuevo = [];
  $scope.periodos = [];

  //socio
  //anadir
  $scope.addProyecto = function (proyecto_nuevo) {
    let proyecto_push = angular.copy(proyecto_nuevo);
    console.log(proyecto_push);
    $scope.proyectos.push(proyecto_push);
    $scope.proyecto_nuevo = [{
      proyecto_id: 0,
      proyecto_nombre: "",
      proyecto_vacantes: 1,
      proyecto_horas_acreditar: "240",
      proyecto_objetivo: "",
      proyecto_perfil: "",
      proyecto_responsable_nombre: "",
      proyecto_responsable_puesto: "",
      proyecto_responsable_correo: "",
      proyecto_responsable_telefono: "",
      proyecto_disponible_whatsapp: "",
      proyecto_consideraciones: "",
      proyecto_estatus: "Disponible para renovar",
      proyecto_archivado: 1,
      proyecto_actividades: [],
      proyecto_horarios_trabajo: [],
      proyecto_lugares_servicio: [],
      proyecto_carreras: [],
      proyecto_estilos_trabajo: []
    }];
  };

  $scope.addActividad = function (actividad, actividad_nueva) {
    if (actividad_nueva.actividad_descripcion && actividad_nueva.actividad_duracion) {
      actividad.push({
        actividad_id: "-1",
        actividad_duracion: actividad_nueva.actividad_duracion,
        actividad_descripcion: actividad_nueva.actividad_descripcion
      });
      console.log("Actividad añadida");
      console.log(actividad);
      $scope.actividad_nueva.actividad_duracion = null;
      $scope.actividad_nueva.actividad_descripcion = null;
    } else {
      alert("Por favor defina una actividad con su duración y descripción");
    }
  };

  $scope.addEstiloTrabajo = function (estilo_trabajo, estilo_trabajo_nuevo) {
    //Entramos al if si nuevo esta definido

    if (estilo_trabajo_nuevo.estilo_trabajo_nombre) {
      //Limpiamos nuevo para quitarle los espacios en blanco
      estilo_trabajo_nuevo.estilo_trabajo_nombre = estilo_trabajo_nuevo.estilo_trabajo_nombre.trim();
      var estilo_trabajo_existe = false;
      //Revisamos si nuevo ya esta registrado
      for (x in estilo_trabajo) {
        if (estilo_trabajo_nuevo.estilo_trabajo_nombre == estilo_trabajo[x].estilo_trabajo_nombre) {
          estilo_trabajo_existe = true;
        }
      }
      //Entra al if si nuevo no esta registrado
      if (!estilo_trabajo_existe) {
        estilo_trabajo.push({
          estilo_trabajo_nombre: estilo_trabajo_nuevo.estilo_trabajo_nombre
        });
      } else {
        alert("Por favor eliga un estilo de trabajo que no este registrado");
      }
    } else {
      alert("Por favor eliga un estilo de trabajo");
    }
  };


  
  $scope.addHorarioTrabajo = function (horario_trabajo, horario_trabajo_nuevo) {
    //Entramos al if si nuevo esta definido

    if (horario_trabajo_nuevo.horario_trabajo_nombre) {
      //Limpiamos nuevo para quitarle los espacios en blanco
      horario_trabajo_nuevo.horario_trabajo_nombre = horario_trabajo_nuevo.horario_trabajo_nombre.trim();
      var horario_trabajo_existe = false;
      //Revisamos si nuevo ya esta registrado
      for (x in horario_trabajo) {
        if (horario_trabajo_nuevo.horario_trabajo_nombre == horario_trabajo[x].horario_trabajo_nombre) {
          horario_trabajo_existe = true;
        }
      }
      //Entra al if si nuevo no esta registrado
      if (!horario_trabajo_existe) {
        horario_trabajo.push({
          horario_trabajo_nombre: horario_trabajo_nuevo.horario_trabajo_nombre
        });
      } else {
        alert("Por favor defina eliga un horario de trabajo que no este registrado");
      }
    } else {
      alert("Por favor defina eliga un horario de trabajo");
    }
  };
  $scope.addLugarServicio = function (lugar_servicio, lugar_servicio_nuevo) {
    //Entramos al if si nuevo esta definido

    if (lugar_servicio_nuevo.lugar_servicio_nombre) {
      //Limpiamos nuevo para quitarle los espacios en blanco
      lugar_servicio_nuevo.lugar_servicio_nombre = lugar_servicio_nuevo.lugar_servicio_nombre.trim();
      var lugar_servicio_existe = false;
      //Revisamos si nuevo ya esta registrado
      for (x in lugar_servicio) {
        if (lugar_servicio_nuevo.lugar_servicio_nombre == lugar_servicio[x].lugar_servicio_nombre) {
          lugar_servicio_existe = true;
        }
      }
      //Entra al if si nuevo no esta registrado
      if (!lugar_servicio_existe) {
        lugar_servicio.push({
          lugar_servicio_nombre: lugar_servicio_nuevo.lugar_servicio_nombre
        });
      } else {
        alert("Por favor eliga un lugar de servicio que no este registrado");
      }
    } else {
      alert("Por favor eliga un lugar de servicio");
    }
  };
  $scope.addCarrera = function (carrera, carrera_nueva) {
    //Hay que limpiar el nombre de la carrera porque trae espacios en blanco
    carrera_nueva.carrera_nombre = carrera_nueva.carrera_nombre.trim();
    //Entra al if porque se selecionó una carrera del menu, lo cual hace que este definida
    if (carrera_nueva.carrera_nombre) {
      var carrera_existe = false;
      //Iteramos sobre las carreras del proyecto para ver si ya esta registrada la carrera selecionada
      for (x in carrera) {
        if (carrera_nueva.carrera_nombre == carrera[x].carrera_nombre) {
          carrera_existe = true;
        }
      }
      //Entra al if si la carrera no esta registrada
      if (!carrera_existe) {
        //Iteramos sobre el combo de carreras para asignar la descripción de la carrera
        for (x in $scope.carreras_menu) {
          if (carrera_nueva.carrera_nombre == $scope.carreras_menu[x].carrera_nombre) {
            carrera_nueva.carrera_descripcion = $scope.carreras_menu[x].carrera_descripcion;
          }
        }
        //Ahora que se tiene tanto la descripcion como el nombre, se le hace un push a las carreras del proyecto
        carrera.push({
          carrera_nombre: carrera_nueva.carrera_nombre,
          carrera_descripcion: carrera_nueva.carrera_descripcion
        });
        $scope.carrera_nueva.carrera_nombre = null;
        $scope.carrera_nueva.carrera_descripcion = null;
        console.log("Carrera añadida");
        console.log(carrera);
        console.log($scope.proyectos);
        //Entra al else porque la carrera ya esta registrada en el proyecto
      } else {
        alert("Por favor seleccione una carrera que no esté registrada");
      }
      //Entra al else porque no se seleccionó ninguna carrera del combo
    } else {
      alert("Por favor seleccione una carrera");
    }
  };

  //quitar
  $scope.removeActividad = function (actividad, proyecto) {
    proyecto.proyecto_actividades.splice(proyecto.proyecto_actividades.indexOf(actividad), 1);
    console.log("Actividad removida");
    console.log(actividad);
  };
  $scope.removeCarrera = function (carrera, proyecto) {
    proyecto.proyecto_carreras.splice(proyecto.proyecto_carreras.indexOf(carrera), 1);
    console.log("Carrera removida");
    console.log(carrera);
  };
  $scope.removeEstiloTrabajo = function (estilo_trabajo, proyecto) {
    proyecto.proyecto_estilos_trabajo.splice(proyecto.proyecto_estilos_trabajo.indexOf(estilo_trabajo), 1);
    console.log("Estilo removido");
    console.log(estilo_trabajo);
  };
  $scope.removeHorarioTrabajo = function (horario_trabajo, proyecto) {
    proyecto.proyecto_horarios_trabajo.splice(proyecto.proyecto_horarios_trabajo.indexOf(horario_trabajo), 1);
    console.log("Horario removido");
    console.log(horario_trabajo);
  };
  $scope.removeLugarServicio = function (lugar_servicio, proyecto) {
    proyecto.proyecto_lugares_servicio.splice(proyecto.proyecto_lugares_servicio.indexOf(lugar_servicio), 1);
    console.log("Lugar removido");
    console.log(lugar_servicio);
  };

  $scope.removeProyecto = function (proyecto) {
    $scope.proyectos.splice($scope.proyectos.indexOf(proyecto), 1);
    console.log(proyecto);
  };

  //renovar proyecto
  $scope.renovarProyecto = function (proyecto) {
    //$scope.proyectos.splice($scope.proyectos.indexOf(proyecto), 1);
    console.log(proyecto);
  };

  //mostrar descripcion de carrera
  $scope.mostrarCarreraDescripcion = function (carrera_nueva) {
    //Iteramos sobre el combo de carreras para asignar la descripción de la carrera
    if (angular.isDefined(carrera_nueva.carrera_nombre)) {
      for (x in $scope.carreras_menu) {
        //Para asignar la descripción, debemos de limpiar el nombre de la carrera porque trae espacios en blanco
        if (carrera_nueva.carrera_nombre.trim() == $scope.carreras_menu[x].carrera_nombre) {
          //Se asigna la descripción para que se muestre en la pantalla del usuario. 
          $scope.carrera_nueva.carrera_descripcion = $scope.carreras_menu[x].carrera_descripcion;
        }
      }
    }
  };

  //guardar edicion de proyectos
  $scope.saveProyectos = function () {
    console.log($scope.proyectos);
    var url = "mvc/models/proyectos/model_proyectos_organizacion_upd.php";
    $http.post(url, $scope.proyectos).then(function (response) {
      $http.get("mvc/models/proyectos/model_socio_proyectos_qry.php")
        .then(function (response) {
          $scope.proyectos = response.data;
          $scope.proyectos_backup = angular.copy($scope.proyectos);
          console.log(response.data);
        });

    });
  };

  //restaurar proyectos
  $scope.restoreProyecto = function () {
    console.log("Restore proyect");
    $scope.proyectos = angular.copy($scope.proyectos_backup);
  };


  //admin



  //consultar proyectos de la organizacion seleccionada
  $scope.consultar_proyectos_organizacion = function () {
    var organizacion_id = $scope.organizacion_seleccionada.organizacion_id;
    console.log(organizacion_id);
    $http({
      url: 'mvc/models/proyectos/model_proyectos_organizacion_admin_qry.php',
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: 'organizacion_id=' + organizacion_id
    }).then(function (response) {
      console.log(response.data);
      $scope.proyectos = response.data;
    })
  }

  //menus
  $http.get("mvc/models/proyectos/model_periodos_qry.php")
    .then(function (response) {
      $scope.periodos = response.data;
    });
  $http.get("mvc/models/proyectos/model_organizaciones_qry.php")
    .then(function (response) {
      $scope.organizaciones = response.data;
    });
  $scope.carreras_menu = [];
  $http.get("mvc/models/proyectos/model_carreras_qry.php")
    .then(function (response) {
      $scope.carreras_menu = response.data;
    });
  $scope.estilos_trabajo = [];
  $http.get("mvc/models/proyectos/model_estilos_qry.php")
    .then(function (response) {
      $scope.estilos_trabajo_menu = response.data;
    });
  $scope.horarios_trabajo = [];
  $http.get("mvc/models/proyectos/model_horarios_qry.php")
    .then(function (response) {
      $scope.horarios_trabajo_menu = response.data;
    });
  $scope.lugares_servicio = [];
  $http.get("mvc/models/proyectos/model_lugares_qry.php")
    .then(function (response) {
      $scope.lugares_servicio_menu = response.data;
    });
  $scope.horas_acreditar_menu = [{
      hora: "120"
    },
    {
      hora: "240"
    }
  ];
});