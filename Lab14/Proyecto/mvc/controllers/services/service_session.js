app.service('user', function () {
    var username;
    var loggedin = false;
    var id;
    var correo;
    var rol;
    var privilegios = [];
    var rol_primary;
    var rol_secondary;
    var nav_rol;

    this.setName = function (name) {
        username = name;
    };
    this.getName = function () {
        return username;
    };

    this.setID = function (userID) {
        id = userID;
    };
    this.getID = function () {
        return id;
    };

    this.setCorreo = function (userCorreo) {
        correo = userCorreo;
    };
    this.getCorreo = function () {
        return correo;
    };

    this.setRol = function (userRol) {
        rol = userRol;
    };
    this.getRol = function () {
        return rol;
    };

    this.setPrivilegios = function (userPrivilegios) {
        privilegios = userPrivilegios;
    };
    this.getPrivilegios = function () {
        return privilegios;
    };

    this.setRolPrimary = function (userRolPrimary) {
        rol_primary = userRolPrimary;
    };
    this.getRolPrimary = function () {
        return rol_primary;
    };

    this.setRolSecondary = function (userRolSecondary) {
        rol_secondary = userRolSecondary;
    };
    this.getRolSecondary = function () {
        return rol_secondary;
    };

    this.setNavRol = function (userNavRol) {
        nav_rol = userNavRol;
    };
    this.getNavRol = function () {
        return nav_rol;
    };


    this.isUserLoggedIn = function () {
        if (!!localStorage.getItem('login')) {
            loggedin = true;
            var data = JSON.parse(localStorage.getItem('login'));
            username = data.username;
            id = data.id;
            correo = data.correo;
            rol = data.rol;
            privilegios = data.privilegios;
            switch (rol) {
                case "1":
                    rol_primary = "deep-purple"
                    rol_secondary = "orange-accent-1"
                    nav_rol = "admin"
                    break;
                case "2":
                    rol_primary = "pink-accent-1"
                    rol_secondary = "teal-accent-1"
                    nav_rol = "socio"
                    break;
            }
        }
        return loggedin;
    };
    this.saveData = function (data) {
        username = data.user;
        correo = data.correo;
        rol = data.rol_id;
        id = data.id;
        privilegios = data.privilegios;
        loggedin = true;
        switch (rol) {
            case "1":
                rol_primary = "deep-purple"
                rol_secondary = "orange-accent-1"
                nav_rol = "admin"
                break;
            case "2":
                rol_primary = "pink-accent-1"
                rol_secondary = "teal-accent-1"
                nav_rol = "socio"
                break;
        }
        localStorage.setItem('login', JSON.stringify({
            username: username,
            id: id,
            correo: correo,
            rol: rol,
            privilegios: privilegios,
            rol_primary: rol_primary,
            rol_secondary: rol_secondary,
            nav_rol: nav_rol
        }));
    };
    this.clearData = function () {
        localStorage.removeItem('login');
        username = "";
        id = "";
        correo = "";
        loggedin = false;
        rol = "";
        privilegios = [];
        rol_primary = ""
        rol_secondary = ""
        nav_rol = ""
    }
})


















app.run(function ($rootScope, user, $location) {
    var lastDigestRun = new Date();
    setInterval(function () {
        var now = Date.now();
        if (now - lastDigestRun > 10 * 60 * 1000) {
            user.clearData();
            $location.path('/');
        }
    }, 60 * 1000);

    $rootScope.$watch(function () {
        lastDigestRun = new Date();
    });
});