<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
if (!isset($_POST)) {
	die();
}
$response = [];
$username = mysqli_real_escape_string($con, $_POST['username']);
$password = mysqli_real_escape_string($con, $_POST['password']);
$query = "	SELECT * 
			FROM usuarios 
			WHERE usuario_login='$username' 
			AND usuario_contraseña='$password'";
$result = mysqli_query($con, $query);
session_start();
if (mysqli_num_rows($result) > 0) {
	$rs = mysqli_fetch_assoc($result);
	$response['rol_id'] = $rs['rol_id'];
	$response['status'] = 'loggedin';
	$response['user'] = $rs['usuario_nombre'];
	$response['id'] = md5(uniqid());
	$response['correo'] = $rs['usuario_correo'];
	$_SESSION['id'] = $response['id'];
	$_SESSION['rol_id'] = $rs['rol_id'];
	$_SESSION['usuario_nombre'] = $rs['usuario_nombre'];
	$usuario_id = $rs['usuario_id'];
	$rol_id = $rs['rol_id'];
	if ($rs['rol_id'] == 2) {
		$query = "	SELECT * 
					FROM `organizaciones` 
					WHERE usuario_id='$usuario_id'";
		$result = mysqli_query($con, $query);
		$rs = mysqli_fetch_assoc($result);
		$_SESSION['organizacion_id'] = $rs['organizacion_id'];
		$_SESSION['organizacion_nombre'] = $rs['organizacion_nombre'];
	}
	$query = ("	SELECT p.privilegio_nombre 
				FROM roles_privilegios rp, privilegios p
				WHERE rp.privilegio_id = p.privilegio_id
				AND rol_id='$rol_id'");
	$result = mysqli_query($con, $query);
	$response_privilegios = [];
	$i = 0;
	while ($rs = mysqli_fetch_assoc($result)) {
		$response_privilegios[$i] = $rs['privilegio_nombre'];
		$i += 1;
	}
	$_SESSION['privilegios'] = $response_privilegios;
	$response['privilegios'] = $response_privilegios;
} else {
	$response['status'] = 'error';
}
mysqli_close($con);
echo json_encode($response);
