<?php
include '../util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
session_start();
//Falta poner la id correspondiente
$organizacion_id = $_SESSION['organizacion_id'];
$con = connect_db();
if (!isset($_POST)) {
    die();
}
$postdata = file_get_contents("php://input");
$proyectos = json_decode($postdata, true);


$lista_proyectos = "";
foreach ($proyectos as $proyecto) {
    $proyecto_id =                      $proyecto['proyecto_id'];
    $proyecto_nombre =                  $proyecto['proyecto_nombre'];
    $proyecto_vacantes =                $proyecto['proyecto_vacantes'];
    $proyecto_horas_acreditar =         $proyecto['proyecto_horas_acreditar'];
    $proyecto_objetivo =                $proyecto['proyecto_objetivo'];
    $proyecto_perfil =                  $proyecto['proyecto_perfil'];
    $proyecto_responsable_nombre =      $proyecto['proyecto_responsable_nombre'];
    $proyecto_responsable_puesto =      $proyecto['proyecto_responsable_puesto'];
    $proyecto_responsable_correo =      $proyecto['proyecto_responsable_correo'];
    $proyecto_responsable_telefono =    $proyecto['proyecto_responsable_telefono'];
    $proyecto_disponible_whatsapp =     $proyecto['proyecto_disponible_whatsapp'];
    $proyecto_consideraciones =         $proyecto['proyecto_consideraciones'];
    $proyecto_estatus =                 $proyecto['proyecto_estatus'];
    $proyecto_archivado =               $proyecto['proyecto_archivado'];
    $query = "SELECT * FROM proyectos WHERE proyecto_id = '$proyecto_id'";
    $result = mysqli_query($con, $query);
    if (mysqli_num_rows($result) > 0) {
        //el proyecto ya existe, se hace una atualizacion 
        $query_proyectos = ("   UPDATE proyectos p
                                SET 
                                      p.proyecto_nombre = '$proyecto_nombre',
                                      p.proyecto_vacantes = '$proyecto_vacantes',
                                      p.proyecto_horas_acreditar = '$proyecto_horas_acreditar',
                                      p.proyecto_objetivo = '$proyecto_objetivo',
                                      p.proyecto_perfil = '$proyecto_perfil',
                                      p.proyecto_responsable_nombre = '$proyecto_responsable_nombre',
                                      p.proyecto_responsable_puesto = '$proyecto_responsable_puesto',
                                      p.proyecto_responsable_correo = '$proyecto_responsable_correo',
                                      p.proyecto_responsable_telefono = '$proyecto_responsable_telefono',
                                      p.proyecto_disponible_whatsapp = '$proyecto_disponible_whatsapp',
                                      p.proyecto_consideraciones = '$proyecto_consideraciones',
                                      p.proyecto_estatus = '$proyecto_estatus' /*,
                                      p.proyecto_archivado = '$proyecto_archivado' */
                                WHERE p.proyecto_id = '$proyecto_id'");
        mysqli_query($con, $query_proyectos);
        //actualizar actividades
        $lista_actvidades = "";
        foreach ($proyecto['proyecto_actividades'] as $actividades) {
            $actividad_id = $actividades['actividad_id'];
            $actividad_descripcion = $actividades['actividad_descripcion'];
            $actividad_duracion = $actividades['actividad_duracion'];
            $query = "  SELECT * 
                        FROM actividades 
                        WHERE proyecto_id = '$proyecto_id'
                        AND actividad_id = '$actividad_id'";
            $result = mysqli_query($con, $query);
            if (mysqli_num_rows($result) > 0) {
                //actividad existe
                $query_actividades = (" UPDATE actividades a
                                        SET 
                                            a.actividad_duracion = '$actividad_duracion',
                                            a.actividad_descripcion ='$actividad_descripcion'                                           
                                        WHERE a.proyecto_id = '$proyecto_id'
                                        AND a.actividad_id = '$actividad_id'");
                mysqli_query($con, $query_actividades);
            } else {
                //actividad nueva
                $query_actividades = (" INSERT INTO actividades
                                        VALUES
                                        (   0,
                                            '$proyecto_id',
                                            '$actividad_descripcion',
                                            '$actividad_duracion'
                                        )");
                mysqli_query($con, $query_actividades);
                $query_actividades = (" SELECT max(actividad_id) 
                                        FROM actividades
                                        WHERE a.proyecto_id = '$proyecto_id'
                                    ");
                $actividad_id = mysqli_query($con, $query_actividades);
            }
            //lista de actividades en el upd para delete
            $lista_actvidades .= "$actividad_id" . ",";
        }
        $lista_actvidades = substr($lista_actvidades, 0, strlen($lista_actvidades) - 1);
        //delete actividades
        $query_actividades = (" DELETE 
                                FROM actividades
                                WHERE proyecto_id = '$proyecto_id'
                                AND actividad_id NOT IN(" . $lista_actvidades . ")");
        mysqli_query($con, $query_actividades);
    } else {
        //el proyecto es nuevo, se inserta el proyecto
        $query_proyectos = ("INSERT INTO proyectos 
                                VALUES
                                (   
                                    0,
                                    '$organizacion_id',
                                    '$proyecto_nombre',
                                    '$proyecto_vacantes',
                                    '$proyecto_horas_acreditar',
                                    '$proyecto_objetivo',
                                    '$proyecto_perfil', 
                                    '$proyecto_responsable_nombre',
                                    '$proyecto_responsable_puesto',   
                                    '$proyecto_responsable_correo',
                                    '$proyecto_responsable_telefono',
                                    '$proyecto_disponible_whatsapp',
                                    '$proyecto_consideraciones',  
                                    '$proyecto_estatus',                              
                                    '$proyecto_archivado'
                                )
                                ");
        mysqli_query($con, $query_proyectos);
        $query_proyectos = ("   SELECT max(proyecto_id) 
                                FROM proyectos
                                WHERE organizacion_id = '$organizacion_id'
                                    ");
        $proyecto_id = mysqli_query($con, $query_proyectos);
    }
    //lista de actividades en el upd para delete
    $lista_proyectos .= "$proyecto_id" . ",";
}
$lista_proyectos = substr($lista_proyectos, 0, strlen($lista_proyectos) - 1);
//delete actividades
$query_proyectos = ("   DELETE 
                        FROM proyectos
                        WHERE proyecto_id NOT IN(".$lista_proyectos.")");
mysqli_query($con, $query_proyectos);


echo json_encode($proyecto);
