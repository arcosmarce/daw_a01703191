<style>
    input {
        text-align: center;
    }

    select {
        text-align: center;
    }
</style>

<table class="table">
    <tbody ng-repeat="proyecto in proyecto_nuevo">

        <tr class="text-center">

            <!--informacion basica-->
            <th scope="row">
                <input ng-model="proyecto.proyecto_nombre" class="form-control">
            </th>
            <td>
                <div class="d-flex justify-content-center">
                    <input class="form-control" type="number" string-to-number ng-model="proyecto.proyecto_vacantes"
                        min="1" max="69">
                </div>
            </td>
            <td>
                <div class="d-flex justify-content-center">
                    <select class="form-control s" ng-model="proyecto.proyecto_horas_acreditar">
                        <option ng-repeat="hora in horas_acreditar_menu">{{hora.hora}}</option>
                    </select>
                </div>
            </td>
            <td>
                <div class="d-flex justify-content-center align-items-center">
<<<<<<< HEAD:Proyecto/components/partials/proyecto/_proyecto_nuevo.html
                    <div class="flex-item  border border-light-blue-accent-1 p-2 rounded bg-light-blue-accent-1">
=======
                    <div class="flex-item  border border-grey-lighten-2 p-2 rounded ">
>>>>>>> 8f622b7becd9e0c7cace4be31042a6739e2e12f7:Proyecto/mvc/vistas/components/proyectos/component_proyectos_agregar.php
                        {{proyecto.proyecto_estatus}}
                    </div>
                </div>
            </td>
            <!--end informacion basica-->
            <!--botones-->
            <td>
                <!--boton anadir-->
                <button class="btn btn-cyan-lighten-4 btn-circle btn-sm" role="button" data-toggle="tooltip"
                    data-placement="top" title="Añadir proyecto" ng-click="addProyecto(proyecto)">
                    <i class="fas fa-plus">
                    </i>
                </button>
                <!--end boton anadir-->
                <!--boton detalles-->
                <button type="button" class="btn btn-circle btn-sm" data-toggle="collapse"
                    data-target="#detalles{{proyecto.proyecto_id}}" data-toggle="tooltip" data-placement="top"
                    title="Detalles" aria-expanded="false" aria-controls="detalles{{proyecto.proyecto_id}}">
                    <i class="fas fa-chevron-down js-rotate-if-collapsed">
                    </i>
                </button>
                <!--end boton detalles-->
            </td>
            <!--end botones-->
        </tr>

        <!----------Detalles---------->
        <tr class="collapse" id="detalles{{proyecto.proyecto_id}}">
            <td colspan="5">

                <!--row descripcion y contacto-->
                <div class="row p-1">
                    <!--col descripcion-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-green-pastel pt-4">
                                    <h4 class="card-title">Descripcion General</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
<<<<<<< HEAD:Proyecto/components/partials/proyecto/_proyecto_nuevo.html
                                    <div ng-include="'components/partials/proyecto/_proyecto_descripcion.html'">
=======
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_descripcion.html'">
>>>>>>> 8f622b7becd9e0c7cace4be31042a6739e2e12f7:Proyecto/mvc/vistas/components/proyectos/component_proyectos_agregar.php
                                    </div>
                                </div>
                                <!--End Card Body-->
                                <!--Card Footer-->
                                <div class="card-footer bg-light">

                                </div>
                                <!--End Card Footer-->
                            </div>
                        </div>
                    </div>
                    <!--end col descripcion-->
                    <!--col contacto-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-yellow-pastel pt-4">
                                    <h4 class="card-title">Contacto</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_contacto.html'"></div>
                                </div>
                                <!--End Card Body-->
                                <!--Card Footer-->
                                <div class="card-footer bg-light">

                                </div>
                                <!--End Card Footer-->
                            </div>
                        </div>
                    </div>
                    <!--col contacto-->
                </div>
                <!--end row descripcion y contacto-->

                <!--row actividades, horario-->
                <div class="row p-1">
                    <!--col actividades-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-indigo-lighten-3 pt-4">
                                    <h4 class="card-title">Actividades</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
<<<<<<< HEAD:Proyecto/components/partials/proyecto/_proyecto_nuevo.html
                                    <div ng-include="'components/partials/proyecto/_proyecto_actividades.html'">
=======
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_actividades.html'">
>>>>>>> 8f622b7becd9e0c7cace4be31042a6739e2e12f7:Proyecto/mvc/vistas/components/proyectos/component_proyectos_agregar.php
                                    </div>
                                </div>
                                <!--End Card Body-->

                            </div>
                        </div>
                    </div>
                    <!--col actividades-->

                    <!--col horario-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-pink-pastel pt-4">
                                    <h4 class="card-title">Horario</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_horarios.html'"></div>
                                </div>
                                <!--End Card Body-->
                                <!--Card Footer-->
                                <div class="card-footer bg-light">

                                </div>
                                <!--End Card Footer-->
                            </div>
                        </div>
                    </div>
                    <!--end col horario-->

                </div>
                <!--end row horario, lugar y estilor-->

                <!--row estilo, lugar-->
                <div class="row p-1">
                    <!--col lugar-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-pink-pastel pt-4">
                                    <h4 class="card-title">Lugar</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_lugares.html'"></div>
                                </div>
                                <!--End Card Body-->
                                <!--Card Footer-->
                                <div class="card-footer bg-light">

                                </div>
                                <!--End Card Footer-->
                            </div>
                        </div>
                    </div>
                    <!--end col lugar-->
                    <!--col estilo-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-pink-pastel pt-4">
                                    <h4 class="card-title">Estilo</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_estilos.html'"></div>
                                </div>
                                <!--End Card Body-->
                                <!--Card Footer-->
                                <div class="card-footer bg-light">

                                </div>
                                <!--End Card Footer-->
                            </div>
                        </div>
                    </div>
                    <!--end col estilo-->
                </div>
                <!--end row estilo, lugar-->

                <!--row carreras-->
                <div class="row p-1">
                    <!--col carreras-->
                    <div class="col-sm">
                        <div class="card w-100 d-flex m-2">
                            <div class="p-2 flex-fill">
                                <!--Card Header-->
                                <div class="card-header bg-purple-pastel pt-4">
                                    <h4 class="card-title">Carreras</h4>
                                </div>
                                <!--End Card Header-->
                                <!--Card Body-->
                                <div class="card-body">
                                    <div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_carreras.html'"></div>
                                </div>
                                <!--End Card Body-->

                            </div>
                        </div>
                    </div>
                    <!--end col carreras-->
                </div>
                <!--row carreras-->
            </td>
        </tr>
        <!----------end detalles---------->
    </tbody>
</table>
<!--row boton agregar proyecto-->