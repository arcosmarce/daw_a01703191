<!--row eliminar-->
<div class="row p-1">
	<div class="col-sm ">
		<div class="container  d-flex justify-content-center align-items-center">

			<div class="card w-50 d-flex border-light">
				<!--Card Header
				<div class="card-header pt-4">
					<h4 class="card-title text-center"></h4>
				</div>
				End Card Header-->
				<!--Card Body-->
				<div class="card-body">

					<form class="card-text rounded">
						<!--Ingresar juego_precio-->
						<div class="form-group">
							<label for="juego_genero">
								<h5 class="card-title text-center">Selecciona Periodo</h5>
							</label>
							<select class="form-control select" ng-model="periodo_seleccionado" ng-options="periodo as periodo.periodo_nombre for periodo in periodos" id="periodo"></select>
						</div>
						<div class="form-group">
							<button type="submit" class="btn btn-{{rol_primary}}" ng-click="consultar_proyectos()">Consultar</button>
						</div>
					</form>

				</div>
				<!--Card Footer
				<div class="card-footer">
					<div class="form-group">
						<button type="submit" class="btn btn-purple" ng-click="consultar_proyectos()">Consultar</button>
					</div>
				</div>
				End Card Footer-->

			</div>
			<!--End Card Body-->


		</div>
	</div>
</div>
<!-- End row eliminar -->

<!--row eliminar-->
<div class="row p-1">
	<div class="col-sm ">



		<table class="table table-light">
			<thead class="bg-{{rol_secondary}} text-dark ">
				<tr>
					<th scope="col">Nombre</th>
					<th scope="col">Vacantes</th>
					<th scope="col">Horas de acreditación</th>
					<th scope="col">Estatus</th>
					<th scope="col">Acciones</th>
				</tr>
			</thead>

			<tr>
				<td><input size="8" class="rounded" ng-model="f.proyecto_nombre"></td>
				<td><input size="8" class="rounded" ng-model="f.proyecto_vacantes"></td>
				<td><input size="8" class="rounded" ng-model="f.proyecto_horas_acreditar"></td>
				<td><input size="8" class="rounded" ng-model="f.proyecto_estatus"></td>
			</tr>




			<tbody ng-repeat="x in proyectos | filter:f">


				<tr>


					<th scope="row">{{x.proyecto_nombre}}</th>
					<td>{{x.proyecto_vacantes}}</td>
					<td>{{x.proyecto_horas_acreditar}}</td>
					<td>{{x.proyecto_estatus}}</td>
					<!--Botones de acciones -->

					<td>

						<button class="btn btn-orange-pastel btn-circle btn-sm p-2 m-1" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Eliminar">
							<i class="fas fa-trash">
							</i>
						</button>
						<button class="btn btn-green-pastel btn-circle btn-sm p-2 m-1" href="#" role="button" data-toggle="tooltip" data-placement="top" title="Archivar">
							<i class="fas fa-archive">
							</i>
						</button>

						<button type="button" class="btn btn-circle btn-sm p-2 m-1" data-toggle="collapse" data-target="#detalles{{x.proyecto_id}}" data-toggle="tooltip" data-placement="top" title="Detalles" aria-expanded="false" aria-controls="detalles{{x.proyecto_id}}">
							<i class="fas fa-chevron-down js-rotate-if-collapsed">
							</i>
						</button>

					</td>
					<!--End Botones de acciones-->
				</tr>




				<!----------Detalles---------->
				<tr class="collapse" id="detalles{{x.proyecto_id}}">
					<td colspan="5">

						<!--row descripcion y contacto-->
						<div class="row p-1">
							<!--col descripcion-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-yellow-pastel pt-4">
											<h4 class="card-title">Descripcion General</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_descripcion.html'"></div>
										</div>
										<!--End Card Body-->
										<!--Card Footer-->
										<div class="card-footer bg-light">

										</div>
										<!--End Card Footer-->
									</div>
								</div>
							</div>
							<!--end col descripcion-->
							<!--col contacto-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-yellow-pastel pt-4">
											<h4 class="card-title">Contacto</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_contacto.html'"></div>
										</div>
										<!--End Card Body-->
										<!--Card Footer-->
										<div class="card-footer bg-light">

										</div>
										<!--End Card Footer-->
									</div>
								</div>
							</div>
							<!--col contacto-->
						</div>
						<!--end row descripcion y contacto-->

						<!--row actividades, horario-->
						<div class="row p-1">
							<!--col actividades-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-indigo-lighten-3 pt-4">
											<h4 class="card-title">Actividades</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_actividades.html'"></div>
										</div>
										<!--End Card Body-->

									</div>
								</div>
							</div>
							<!--col actividades-->

							<!--col horario-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-pink-pastel pt-4">
											<h4 class="card-title">Horario</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_horarios.html'"></div>
										</div>
										<!--End Card Body-->
										<!--Card Footer-->
										<div class="card-footer bg-light">

										</div>
										<!--End Card Footer-->
									</div>
								</div>
							</div>
							<!--end col horario-->

						</div>
						<!--end row horario, lugar y estilor-->

						<!--row estilo, lugar-->
						<div class="row p-1">
							<!--col lugar-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-pink-pastel pt-4">
											<h4 class="card-title">Lugar</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_lugares.html'"></div>
										</div>
										<!--End Card Body-->
										<!--Card Footer-->
										<div class="card-footer bg-light">

										</div>
										<!--End Card Footer-->
									</div>
								</div>
							</div>
							<!--end col lugar-->
							<!--col estilo-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-pink-pastel pt-4">
											<h4 class="card-title">Estilo</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_estilos.html'"></div>
										</div>
										<!--End Card Body-->
										<!--Card Footer-->
										<div class="card-footer bg-light">

										</div>
										<!--End Card Footer-->
									</div>
								</div>
							</div>
							<!--end col estilo-->
						</div>
						<!--end row estilo, lugar-->

						<!--row carreras-->
						<div class="row p-1">
							<!--col carreras-->
							<div class="col-sm">
								<div class="card w-100 d-flex m-2">
									<div class="p-2 flex-fill">
										<!--Card Header-->
										<div class="card-header bg-purple-pastel pt-4">
											<h4 class="card-title">Carreras</h4>
										</div>
										<!--End Card Header-->
										<!--Card Body-->
										<div class="card-body">
											<div ng-include="'mvc/vistas/components/proyectos/partials/_proyecto_carreras.html'"></div>
										</div>
										<!--End Card Body-->

									</div>
								</div>
							</div>
							<!--end col carreras-->
						</div>
						<!--row carreras-->
					</td>
				</tr>
				<!----------end detalles--------->
			</tbody>
		</table>


	</div>
</div>
<!-- End row eliminar -->