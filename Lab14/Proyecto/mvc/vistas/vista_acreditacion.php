<?php include 'components/component_navbar.php';

?>
<main>
	<nav class="navbar navbar-expand-lg navbar-dark bg-{{rol_primary}}" ng-controller="controller_navbar" ng-style="style">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarProyectos" aria-controls="navbarProyectos" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarProyectos">

			<ul class="navbar-nav container mw-100 d-flex align-items-center justify-content-center">

				<li ng-show="isSocio" ng-click="showAcreditacionAnswer()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center ">
						<span class="link-text">Candidatos</span>
					</a>
				</li>

				<li ng-show="isSocio" ng-click="showAcreditacionUpload()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Regulares</span>
					</a>
				</li>

				<li ng-show="isAdmin" ng-click="showAcreditacionConsult()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Consultar</span>
					</a>
				</li>


			</ul>

		</div>
	</nav>

	<div class="container h-100 ">




		<div ng-show="tab_acreditacion_candidatos" class="mt-2 mb-1 p-2 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<div class="container d-flex flex-column justify-content-center">
						<h3 class="p-2">Acreditación Sercivio Social Ciudadano</h3>
					</div>


					<h5>Fecha límite de entrega de Cartas: 15/Febrero/2020</h5>
				</div>
			</div>
			<div class="row p-1">

				<div class="col-sm">
					<div class="container d-flex flex-column justify-content-center">



					</div>
				</div>
			</div>

			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-item p-2">
						<button class="btn btn-{{rol_secondary}} btn-circle btn-xl" role="button" data-toggle="tooltip" data-placement="top" title="Subir Carta">
							<i class="fas fa-file-download fa-3x">
							</i>
						</button>

					</div>
					<div class="flex-item p-2">

						<button class="btn btn-{{rol_primary}} btn-circle btn-xl" role="button" data-toggle="tooltip" data-placement="top" title="Subir Carta">
							<i class="fas fa-file-upload fa-3x">
							</i>
						</button>
					</div>
				</div>
			</div>


			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'mvc/vistas/components/component_acreditacion.php'">
						</div>
					</div>
				</div>
			</div>


		</div>



		<!--seccion agregar proyecto-->
		<div ng-show="tab_acreditacion_regulares" class="mt-2 mb-1 p-2 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<div class="container d-flex flex-column justify-content-center">
						<h3 class="p-2">Acreditación Sercivio Social Ciudadano</h3>
					</div>


					<h5>Fecha límite de entrega de Cartas: 15/Febrero/2020</h5>
				</div>
			</div>
			<div class="row p-1">

				<div class="col-sm">
					<div class="container d-flex flex-column justify-content-center">



					</div>
				</div>
			</div>

			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-item p-2">
						<button class="btn btn-{{rol_secondary}} btn-circle btn-xl" role="button" data-toggle="tooltip" data-placement="top" title="Subir Carta">
							<i class="fas fa-file-download fa-3x">
							</i>
						</button>

					</div>
					<div class="flex-item p-2">

						<button class="btn btn-{{rol_primary}} btn-circle btn-xl" role="button" data-toggle="tooltip" data-placement="top" title="Subir Carta">
							<i class="fas fa-file-upload fa-3x">
							</i>
						</button>
					</div>
				</div>
			</div>


			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'mvc/vistas/components/component_acreditacion.php'">
						</div>
					</div>
				</div>
			</div>
		</div>




		<!--seccion agregar proyecto-->
		<div ng-show="tab_acreditacion_consult" class="mt-2 mb-1 p-2 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<h3 class="p-2">Consultar Cartas de Acreditación</h3>
				</div>

			</div>
			<div class="row p-1">
				<div class="container d-flex flex-column justify-content-center">
					<div class="card w-100 d-flex border-light">
						<div class="card-body">
							<form class="card-text rounded">
								<div class="form-group">
									<label for="juego_genero">
										<h5 class="card-title text-center">Selecciona Periodo</h5>
									</label>
									<select class="form-control select" ng-model="periodo_seleccionada" ng-options="periodo as periodo.periodo_nombre for periodo in periodos"></select>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-{{rol_primary}}">Consultar</button>
								</div>
							</form>
						</div>
					</div>

				</div>

			</div>





		</div>
</main>