<?php include 'components/component_navbar.php';

?>
<main>
	<nav class="navbar navbar-expand-lg navbar-dark bg-{{rol_primary}}" ng-controller="controller_navbar" ng-style="style">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarProyectos" aria-controls="navbarProyectos" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarProyectos">

			<ul class="navbar-nav container mw-100 d-flex align-items-center justify-content-center">

				<li ng-click="showConvenioConsult()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center ">
						<span class="link-text">Consultar</span>
					</a>
				</li>

				<li ng-show="isSocio" ng-click="showConvenioUpload()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Subir</span>
					</a>
				</li>

				<li ng-show="isAdmin" ng-click="showConvenioPlantillaUpload()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Subir Plantilla</span>
					</a>
				</li>


			</ul>

		</div>
	</nav>

	<div class="container h-100 ">




		<div ng-show="tab_convenio_consult && convenio_consult" class="mt-2 mb-1 p-2 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<h3 class="p-2">Consultar Convenio</h3>
				</div>


			</div>

			<div ng-show="isAdmin" class="row p-1">
				<div class="col-sm ">
					<div class="container  d-flex justify-content-center align-items-center">
						<div class="card w-100 d-flex border-light">
							<div class="card-body">
								<form class="card-text rounded">
									<div class="form-group">
										<label for="juego_genero">
											<h5 class="card-title text-center">Selecciona Periodo</h5>
										</label>
										<select class="form-control select" ng-model="periodo_seleccionada" ng-options="periodo as periodo.periodo_nombre for periodo in periodos"></select>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-{{rol_primary}}">Consultar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div ng-show="isSocio" class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'mvc/vistas/components/component_convenio.php'">
						</div>
					</div>
				</div>
			</div>
		</div>



		<!--seccion agregar proyecto-->
		<div ng-show="tab_convenio_upload && convenio_upload" class="mt-2 mb-1 p-2 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<h3 class="p-2">Subir Convenio</h3>
				</div>

			</div>
			<div class="row p-1">
				<div class="container d-flex flex-column justify-content-center">
					<p>
						Recuerde subir su convenio firmado antés de la fecha de fin de registro de proyectos.
					</p>
					<h4>Fin de Registro de Proyectos: 15/Febrero/2020</h4>

					<div id="carouselExampleControls" class="carousel slide m-4" data-ride="carousel">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="w-50" src="resources/images/convenio.jpeg" alt="First slide">
							</div>

						</div>

					</div>
				</div>

			</div>
			<div class="row p-1" ng-show="convenio_upload">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">

						<button class="btn btn-{{rol_secondary}} btn-circle btn-xl" role="button" data-toggle="tooltip" data-placement="top" title="Agregar proyecto">
							<i class="fas fa-file-upload fa-3x">
							</i>
						</button>

					</div>
				</div>
			</div>
		</div>




		<!--seccion agregar proyecto-->
		<div ng-show="tab_convenio_plantilla_upload && convenio_plantilla_upload" class="mt-2 mb-1 p-2 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">

				<div class="col-sm">
					<h3 class="p-2">Subir Plantilla</h3>
				</div>

			</div>
			<div class="row p-1">
				<div class="container d-flex flex-column justify-content-center">
					<p>
						Carge en el sistema la plantilla que será utilizada para generar los convenios del periodo actual.
					</p>


				</div>

			</div>
			<div class="row p-1 m-2" ng-show="convenio_plantilla_upload">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">

						<button class="btn btn-{{rol_secondary}} btn-circle btn-xl" role="button" data-toggle="tooltip" data-placement="top" title="Agregar proyecto">
							<i class="fas fa-file-upload fa-3x">
							</i>
						</button>

					</div>
				</div>
			</div>
		



		</div>
</main>