<?php include 'components/component_navbar.php';

?>
<main>
	<nav class="navbar navbar-expand-lg navbar-dark bg-{{rol_primary}}" ng-controller="controller_navbar" ng-style="style">
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarProyectos" aria-controls="navbarProyectos" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarProyectos">

			<ul class="navbar-nav container mw-100 d-flex align-items-center justify-content-center">

				<li ng-click="showProyectoConsult()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center ">
						<span class="link-text">Consultar</span>
					</a>
				</li>

				<li ng-click="showProyectoCreate()" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Agregar</span>
					</a>
				</li>

				<li ng-click="showProyectoValidate()" ng-show="isAdmin" class="nav-item nav-item-{{nav_rol}} round">
					<a class="nav-link text-center">
						<span class="link-text">Validar</span>
					</a>
				</li>

			</ul>

		</div>
	</nav>

	<div class="container h-100 ">






		<!--privilegios: proyecto_consult-->
		<div ng-show="tab_proyecto_consult && proyecto_consult" class="mt-2 mb-1 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">
				<div class="col-sm">

				</div>
				<div class="col-sm">
					<h3 class="p-2">Proyectos</h3>
				</div>
				<!--botones: reset, update-->
				<div class="col-sm" id="proyecto_registrar">
					<!--privilegios: proyecto_delete, proyecto_update -->
					<div ng-show="proyecto_delete && proyecto_update" class="row p1 h-100">
						<div class="container d-flex justify-content-end align-items-center">
							<div class="p-1">
								<button class="btn btn-{{rol_secondary}} btn-circle btn-md" role="button" data-toggle="tooltip" data-placement="top" title="Revertir cambios" ng_click="restoreProyecto()">
									<i class="fas fa-undo fa-2x">
									</i>
								</button>
							</div>
							<div class="p-1 ">
								<button class="btn btn-{{rol_primary}} btn-circle btn-md" role="button" data-toggle="tooltip" data-placement="top" title="Guardar cambios" ng_click="saveProyectos()">
									<i class="fas fa-save fa-2x">
									</i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--rol: admin-->
			<div ng-show="isAdmin" class="row p-1">
				<div class="col-sm ">
					<div class="container  d-flex justify-content-center align-items-center">
						<div class="card w-100 d-flex border-light">
							<div class="card-body">
								<form class="card-text rounded">
									<div class="form-group">
										<label for="juego_genero">
											<h5 class="card-title text-center">Selecciona Organización</h5>
										</label>
										<select class="form-control select" ng-model="organizacion_seleccionada" ng-options="organizacion as organizacion.organizacion_nombre for organizacion in organizaciones"></select>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-{{rol_primary}}" ng-click="consultar_proyectos_organizacion()">Consultar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--proyectos-->
			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div class="" ng-include="'mvc/vistas/components/proyectos/component_proyectos.php'">
						</div>
					</div>
				</div>
			</div>
		</div>



		<!--seccion agregar proyecto-->
		<div ng-show="tab_proyecto_create && proyecto_create" id="proyecto_agregar" class="mt-2 mb-1 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">
				<div class="col-sm">

				</div>
				<div class="col-sm">
					<h3 class="p-2">Agregar proyecto</h3>
				</div>
				<!--botones: reset, update-->
				<div class="col-sm" id="proyecto_registrar">
					<!--privilegios: proyecto_delete, proyecto_update -->
					<div ng-show="proyecto_delete && proyecto_update" class="row p1 h-100">
						<div class="container d-flex justify-content-end align-items-center">
							<div class="p-1 ">
								<button class="btn btn-{{rol_primary}} btn-circle btn-md" role="button" data-toggle="tooltip" data-placement="top" title="Agregar proyecto" ng_click="addProyecto()">
									<i class="fas fa-plus fa-2x">
									</i>
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--rol: admin-->
			<div ng-show="isAdmin" class="row p-1">
				<div class="col-sm ">
					<div class="container  d-flex justify-content-center align-items-center">
						<div class="card w-100 d-flex border-light">
							<div class="card-body">
								<form class="card-text rounded">
									<div class="form-group">
										<label for="juego_genero">
											<h5 class="card-title text-center">Selecciona Organización</h5>
										</label>
										<select class="form-control select" ng-model="organizacion_seleccionada" ng-options="organizacion as organizacion.organizacion_nombre for organizacion in organizaciones"></select>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-{{rol_primary}}" ng-click="consultar_proyectos_organizacion()">Seleccionar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'mvc/vistas/components/proyectos/component_proyectos.php'"></div>
					</div>
				</div>
			</div>
		</div>
		<!--seccion agregar proyecto-->

		<!--seccion agregar proyecto-->
		<div ng-show="tab_proyecto_validate && proyecto_validate" id="proyecto_agregar" class="mt-2 mb-1 border-bottom border-grey-lighten-1 container ">
			<div class="row p-1">
				<div class="col-sm">
					<h3 class="p-2">Validar Proyectos</h3>
				</div>
			</div>
			<!--rol: admin-->
			<div ng-show="isAdmin" class="row p-1">
				<div class="col-sm ">
					<div class="container  d-flex justify-content-center align-items-center">
						<div class="card w-100 d-flex border-light">
							<div class="card-body">
								<form class="card-text rounded">
									<div class="form-group">
										<label for="juego_genero">
											<h5 class="card-title text-center">Selecciona Organización</h5>
										</label>
										<select class="form-control select" ng-model="organizacion_seleccionada" ng-options="organizacion as organizacion.organizacion_nombre for organizacion in organizaciones"></select>
									</div>
									<div class="form-group">
										<button type="submit" class="btn btn-{{rol_primary}}" >Consultar</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row p-1">
				<div class="container d-flex justify-content-center">
					<div class="flex-fill">
						<div ng-include="'mvc/vistas/components/proyectos/component_proyectos.php'"></div>
					</div>
				</div>
			</div>
		</div>
		<!--seccion agregar proyecto-->








	</div>
</main>