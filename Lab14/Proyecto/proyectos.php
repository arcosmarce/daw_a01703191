<html>

<head>
	<?php include 'partials/_head.html'; ?>
	<title><?php echo basename(__DIR__); ?></title>
</head>

<body>
	<?php include 'partials/_navbar.html'; ?>
	<main ng-app="myApp" >
		<?php include 'partials/_proyectos_consultar.html'; ?>
	</main>
</body>
<?php include 'partials/_footer.html'; ?>

</html>