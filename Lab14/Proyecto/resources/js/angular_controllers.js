var app = angular.module('myApp', []);

app.controller('proyectosCtrl', function ($scope, $http) {
    $http.get("./resources/php_database/proyectos_socios.php")
        .then(function (response) {
            $scope.proyectos = response.data.records;
        });
});

app.controller('actividadesCtrl', function ($scope, $http) {
    $http.get("./resources/php_database/actividades_proyecto.php")
        .then(function (response) {
            $scope.actividades = response.data.records;
        });
});