app.controller('controller_admin_proyectos', function ($scope, $http) {
    //Consultar periodos para mostrar en el filtro de periodo
    $scope.periodos = [];
    $http.get("./resources/server/server_periodos_qry.php")
        .then(function (response) {
            console.log(response.data);
            $scope.periodos = response.data;
        });
    //Consultar proyectos del periodo seleccionado
    $scope.consultar_proyectos = function () {
        var periodo = $scope.periodo_seleccionado.periodo_id;
        console.log(periodo);
        $http({
            url: './resources/server/admin/server_admin_proyectos_qry.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'periodo=' + periodo
        }).then(function (response) {
            console.log(response.data);
            $scope.proyectos = response.data;
        })
    }
});