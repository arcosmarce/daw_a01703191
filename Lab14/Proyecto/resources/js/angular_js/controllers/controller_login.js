app.controller('controller_login', function ($scope, $http, $location, user) {
    $scope.login = function () {
        var username = $scope.username;
        var password = $scope.password;
        console.log(username);
        
        $http({
            url: './resources/server/server_login.php',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: 'username=' + username + '&password=' + password
        }).then(function (response) {
            if (response.data.status == 'loggedin') {
                user.saveData(response.data);
                console.log(response.data);
                //Dependiendo del rol del usuario, mandalo al home de admin o de socio
                switch (parseInt(response.data.rol_id)) {
                    case 1:
                        $location.path('/admin_home');
                        break;
                    case 2:
                        $location.path('/socio_home');
                        break;
                    default:
                        alert(response.data.rol_id);
                        break;
                }
            } else {
                alert('invalid login');
            }
        })
    }
});