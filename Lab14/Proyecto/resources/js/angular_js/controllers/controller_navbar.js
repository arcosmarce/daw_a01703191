app.controller('controller_navbar', function ($scope, user, $location, $http) {
    $scope.user = user.getName();
    $scope.correo = user.getCorreo();
    $scope.rol = user.getRol();
    $scope.logout = function () {
        $http.post("./resources/server/server_session_terminate.php")
            .then(function (response) {
                console.log(response.data);
            });
        user.clearData();
        $location.path('/');
    }
});