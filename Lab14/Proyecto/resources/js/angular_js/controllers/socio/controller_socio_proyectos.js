app.controller('controller_socio_proyectos', function ($scope, $http) {

  $scope.proyectos = [];
  $scope.actividad_nueva = [];
  $scope.carrera_nueva = [];

  $scope.addActividad = function (actividad, actividad_nueva) {
    actividad.push({
      actividad_duracion: actividad_nueva.actividad_duracion,
      actividad_descripcion: actividad_nueva.actividad_descripcion
    });
    actividad_nueva = [];
    console.log("Actividad añadida");
    console.log(actividad);
  };

  $scope.addCarrera = function (carrera, carrera_nueva) {
    for (x in $scope.carreras) {
      if (carrera_nueva.carrera_nombre == $scope.carreras[x].carrera_nombre) {
        carrera_nueva.carrera_descripcion = $scope.carreras[x].carrera_descripcion;
        console.log(carrera_nueva.carrera_descripcion);
      }
    }
    var carrera_existe = false;
    for (x in carrera) {
      console.log(carrera[x].carrera_nombre);
      if (carrera_nueva.carrera_nombre == carrera[x].carrera_nombre) {
        carrera_existe = true;
        console.log(carrera_existe);
      }
    }
    if (!carrera_existe) {
      carrera.push({
        carrera_nombre: carrera_nueva.carrera_nombre,
        carrera_descripcion: carrera_nueva.carrera_descripcion
      });
      carrera_nueva = [];
      console.log("Carrera añadida");
      console.log(carrera);
    } else {
      console.log("Carrera ya existe");
    }
  };

  $scope.removeActividad = function (actividad, proyecto) {
    proyecto.proyecto_actividades.splice(proyecto.proyecto_actividades.indexOf(actividad), 1);
    console.log("Actividad removida");
    console.log(actividad);
  };

  $scope.removeCarrera = function (carrera, proyecto) {
    proyecto.proyecto_carreras.splice(proyecto.proyecto_carreras.indexOf(carrera), 1);
    console.log("Carrera removida");
    console.log(carrera);
  };





  //Consulta proyectos
  $http.get("./resources/server/socio/server_socio_proyectos_qry.php")
    .then(function (response) {
      console.log(response.data);
      $scope.proyectos = response.data;
      $scope.proyectos_backup = angular.copy($scope.proyectos);
      console.log($scope.proyectos_backup);
    });

  //Consultar carreras para mostrar en el filtro de carreras
  $scope.carreras = [];
  $http.get("./resources/server/server_carreras_qry.php")
    .then(function (response) {
      console.log(response.data);
      $scope.carreras = response.data;
    });
  //Combos
  $scope.horas_acreditar_combo = [{
      hora: "120"
    },
    {
      hora: "240"
    }
  ];
  //$scope.actividad_combo = [{actividad_duracion: "" , actividad_descripcion: ""} ];




});