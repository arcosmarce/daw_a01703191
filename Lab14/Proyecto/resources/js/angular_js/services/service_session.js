app.service('user', function () {
    var username;
    var loggedin = false;
    var id;
    var correo;
    var rol;

    this.setName = function (name) {
        username = name;
    };
    this.getName = function () {
        return username;
    };

    this.setID = function (userID) {
        id = userID;
    };
    this.getID = function () {
        return id;
    };

    this.setCorreo = function (userCorreo) {
        correo = userCorreo;
    };
    this.getCorreo = function () {
        return correo;
    };

    this.setRol = function (userRol) {
        rol = userRol;
    };
    this.getRol = function () {
        return rol;
    };
    this.isUserLoggedIn = function () {
        if (!!localStorage.getItem('login')) {
            loggedin = true;
            var data = JSON.parse(localStorage.getItem('login'));
            username = data.username;
            id = data.id;
            correo = data.correo;
            rol = data.rol;
        }
        return loggedin;
    };
    this.saveData = function (data) {
        username = data.user;
        console.log(data.user);
        correo = data.correo;
        rol = data.rol_id;
        id = data.id;
        loggedin = true;
        localStorage.setItem('login', JSON.stringify({
            username: username,
            id: id,
            correo: correo,
            rol: rol
        }));
    };
    this.clearData = function () {
        localStorage.removeItem('login');
        username = "";
        id = "";
        correo = "";
        loggedin = false;
        rol = "";
    }
})

app.run(function ($rootScope, user, $location) {
    var lastDigestRun = new Date();
    setInterval(function () {
        var now = Date.now();
        if (now - lastDigestRun > 10 * 60 * 1000) {
            user.clearData();
            $location.path('/');
        }
    }, 60 * 1000);

    $rootScope.$watch(function () {
        lastDigestRun = new Date();
    });
});