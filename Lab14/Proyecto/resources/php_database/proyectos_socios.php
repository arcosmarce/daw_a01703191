<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$conn = new mysqli("localhost", "root", "", "socios_formadores");
$result = $conn->query("SELECT nombre_proyecto, concat('detalles', proyecto_id) as collapse_id, vacantes, horas_acreditar, estatus FROM proyectos");
$outp = "";
while($rs = $result->fetch_array(MYSQLI_ASSOC)) {
  if ($outp != "") {$outp .= ",";}
  $outp .= '{"nombre":"'  . $rs["nombre_proyecto"] . '",';
  $outp .= '"collapse_id":"' . $rs["collapse_id"]        . '",';
  $outp .= '"vacantes":"' . $rs["vacantes"]        . '",';
  $outp .= '"horas":"'    . $rs["horas_acreditar"] . '",';
  $outp .= '"estatus":"'  . $rs["estatus"]         . '"}';
}
$outp ='{"records":['.$outp.']}';
$conn->close();
echo($outp);
