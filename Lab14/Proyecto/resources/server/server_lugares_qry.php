<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
$query = (" SELECT *
            FROM lugares_servicio");
$result = mysqli_query($con, $query);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result)) {
    $aux['lugar_servicio_id'] =      $rs['lugar_servicio_id'];
    $aux['lugar_servicio_nombre'] =   $rs['lugar_servicio_nombre'];
    $aux['lugar_servicio_descripcion'] =   $rs['lugar_servicio_descripcion'];
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
