<?php
include './util.php';
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
$con = connect_db();
$query_periodos = (" SELECT *
                      FROM periodos as p 
                       ");
$result_periodos = mysqli_query($con, $query_periodos);
$response = [];
$aux = [];
$i = 0;
while ($rs = mysqli_fetch_assoc($result_periodos)) {
    $aux['periodo_id'] =      $rs['periodo_id'];
    $aux['periodo_nombre'] =   $rs['periodo_nombre'];
    $response[$i] = $aux;
    $i += 1;
}
mysqli_close($con);
echo json_encode($response);
