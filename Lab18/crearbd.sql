-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 20-04-2020 a las 22:04:25
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `crearbd`
--

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `coahuilaproveedor`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `coahuilaproveedor` (
`RFC` char(13)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `coahuilaproveedores`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `coahuilaproveedores` (
`RFC` char(13)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entregan`
--

CREATE TABLE `entregan` (
  `clave` int(4) DEFAULT NULL,
  `RFC` varchar(10) DEFAULT NULL,
  `numero` int(4) DEFAULT NULL,
  `fecha` varchar(10) DEFAULT NULL,
  `cantidad` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entregan`
--

INSERT INTO `entregan` (`clave`, `RFC`, `numero`, `fecha`, `cantidad`) VALUES
(1000, 'AAAA800101', 5000, '8/7/1998', 165),
(1010, 'BBBB800101', 5001, '3/5/2000', 528),
(1020, 'CCCC800101', 5002, '29/7/2001', 582),
(1030, 'DDDD800101', 5003, '21/2/1998', 202),
(1040, 'EEEE800101', 5004, '11/12/1999', 263),
(1050, 'FFFF800101', 5005, '14/10/2000', 503),
(1060, 'GGGG800101', 5006, '4/5/2000', 324),
(1070, 'HHHH800101', 5007, '23/2/1998', 2),
(1080, 'AAAA800101', 5008, '12/1/1999', 86),
(1090, 'BBBB800101', 5009, '1/8/2000', 73),
(1100, 'CCCC800101', 5010, '10/9/2001', 699),
(1110, 'DDDD800101', 5011, '28/6/2003', 368),
(1120, 'EEEE800101', 5012, '3/4/2001', 215),
(1130, 'FFFF800101', 5013, '4/4/2002', 63),
(1140, 'GGGG800101', 5014, '12/7/2001', 219),
(1150, 'HHHH800101', 5015, '4/3/1999', 458),
(1160, 'AAAA800101', 5016, '1/6/2000', 162),
(1170, 'BBBB800101', 5017, '4/2/1998', 180),
(1180, 'CCCC800101', 5018, '14/6/2002', 407),
(1190, 'DDDD800101', 5019, '12/9/1998', 94),
(1200, 'EEEE800101', 5000, '5/3/2000', 177),
(1210, 'FFFF800101', 5001, '5/11/1999', 43),
(1220, 'GGGG800101', 5002, '1/2/2003', 24),
(1230, 'HHHH800101', 5003, '6/1/2003', 530),
(1240, 'AAAA800101', 5004, '12/1/2003', 152),
(1250, 'BBBB800101', 5005, '8/7/2002', 71),
(1260, 'CCCC800101', 5006, '10/5/1999', 460),
(1270, 'DDDD800101', 5007, '10/3/1999', 506),
(1280, 'EEEE800101', 5008, '29/7/2002', 107),
(1290, 'FFFF800101', 5009, '8/1/1998', 132),
(1300, 'GGGG800101', 5010, '8/1/2003', 119),
(1310, 'HHHH800101', 5011, '12/4/2002', 72),
(1320, 'AAAA800101', 5012, '6/1/2003', 698),
(1330, 'BBBB800101', 5013, '10/12/1998', 554),
(1340, 'CCCC800101', 5014, '2/12/2002', 324),
(1350, 'DDDD800101', 5015, '9/5/1999', 272),
(1360, 'EEEE800101', 5016, '7/11/2000', 364),
(1370, 'FFFF800101', 5017, '12/2/2000', 44),
(1380, 'GGGG800101', 5018, '3/3/2002', 302),
(1390, 'HHHH800101', 5019, '12/1/2003', 107),
(1400, 'AAAA800101', 5000, '12/3/2002', 382),
(1410, 'BBBB800101', 5001, '5/2/2000', 601),
(1420, 'CCCC800101', 5002, '7/4/1998', 603),
(1430, 'DDDD800101', 5003, '2/9/1999', 576),
(1000, 'AAAA800101', 5019, '8/8/1999', 254),
(1010, 'BBBB800101', 5018, '29/3/2002', 523),
(1020, 'CCCC800101', 5017, '4/2/1999', 8),
(1030, 'DDDD800101', 5016, '5/11/2000', 295),
(1040, 'EEEE800101', 5015, '12/7/2002', 540),
(1050, 'FFFF800101', 5014, '7/3/1999', 623),
(1060, 'GGGG800101', 5013, '2/1/2000', 692),
(1070, 'HHHH800101', 5012, '3/12/1999', 503),
(1080, 'AAAA800101', 5011, '7/11/2002', 699),
(1090, 'BBBB800101', 5010, '3/1/1998', 421),
(1100, 'CCCC800101', 5009, '6/8/2000', 466),
(1110, 'DDDD800101', 5008, '10/5/1999', 337),
(1120, 'EEEE800101', 5007, '7/7/2001', 692),
(1130, 'FFFF800101', 5006, '6/7/2002', 562),
(1140, 'GGGG800101', 5005, '2/9/2001', 583),
(1150, 'HHHH800101', 5004, '10/8/2001', 453),
(1160, 'AAAA800101', 5019, '9/6/1999', 244),
(1170, 'BBBB800101', 5018, '12/11/1999', 53),
(1180, 'CCCC800101', 5017, '3/3/2001', 334),
(1190, 'DDDD800101', 5016, '4/2/2000', 356),
(1200, 'EEEE800101', 5015, '6/5/2000', 585),
(1210, 'FFFF800101', 5014, '3/11/2001', 70),
(1220, 'GGGG800101', 5013, '4/7/2002', 658),
(1230, 'HHHH800101', 5012, '9/12/2002', 312),
(1240, 'AAAA800101', 5011, '12/8/2000', 366),
(1250, 'BBBB800101', 5010, '4/4/2002', 691),
(1260, 'CCCC800101', 5009, '9/8/1999', 631),
(1270, 'DDDD800101', 5008, '3/9/1997', 546),
(1280, 'EEEE800101', 5007, '3/2/2000', 331),
(1290, 'FFFF800101', 5006, '8/2/2001', 279),
(1300, 'GGGG800101', 5005, '10/6/2002', 521),
(1310, 'HHHH800101', 5019, '2/10/2002', 199),
(1320, 'AAAA800101', 5018, '7/3/2000', 413),
(1330, 'BBBB800101', 5017, '11/8/2000', 93),
(1340, 'CCCC800101', 5016, '6/11/1998', 674),
(1350, 'DDDD800101', 5015, '2/8/1999', 261),
(1360, 'EEEE800101', 5014, '7/4/2002', 265),
(1370, 'FFFF800101', 5013, '8/4/2000', 575),
(1380, 'GGGG800101', 5012, '8/7/1998', 645),
(1390, 'HHHH800101', 5011, '8/11/2001', 697),
(1400, 'AAAA800101', 5010, '5/6/1998', 116),
(1410, 'BBBB800101', 5009, '3/5/2002', 467),
(1420, 'CCCC800101', 5008, '2/8/2000', 278),
(1430, 'DDDD800101', 5007, '9/1/1998', 13),
(1000, 'AAAA800101', 5019, '6/4/2000', 7),
(1010, 'BBBB800101', 5018, '10/11/2000', 667),
(1020, 'CCCC800101', 5017, '4/5/2001', 478),
(1030, 'DDDD800101', 5016, '9/4/1998', 139),
(1040, 'EEEE800101', 5015, '10/6/2000', 546),
(1050, 'FFFF800101', 5014, '4/6/1999', 90),
(1060, 'GGGG800101', 5013, '10/7/2000', 47),
(1070, 'HHHH800101', 5012, '1/4/2000', 516),
(1080, 'AAAA800101', 5011, '1/6/2003', 429),
(1090, 'BBBB800101', 5010, '6/6/1998', 612),
(1100, 'CCCC800101', 5009, '7/5/2002', 523),
(1110, 'DDDD800101', 5008, '9/2/2000', 292),
(1120, 'EEEE800101', 5007, '12/3/1998', 167),
(1130, 'FFFF800101', 5006, '6/5/1999', 673),
(1140, 'GGGG800101', 5005, '7/2/1998', 651),
(1150, 'HHHH800101', 5004, '1/9/2003', 270),
(1160, 'AAAA800101', 5019, '8/2/2002', 665),
(1170, 'BBBB800101', 5018, '6/8/2001', 517),
(1180, 'CCCC800101', 5017, '1/6/2001', 216),
(1190, 'DDDD800101', 5016, '7/3/2003', 622),
(1200, 'EEEE800101', 5015, '10/7/2000', 653),
(1210, 'FFFF800101', 5014, '6/9/2001', 479),
(1220, 'GGGG800101', 5013, '8/2/2001', 653),
(1230, 'HHHH800101', 5012, '8/3/1999', 115),
(1240, 'AAAA800101', 5011, '5/8/2003', 549),
(1250, 'BBBB800101', 5010, '8/5/1998', 690),
(1260, 'CCCC800101', 5009, '10/2/2003', 2),
(1270, 'DDDD800101', 5008, '12/4/2002', 324),
(1280, 'EEEE800101', 5007, '7/12/2002', 448),
(1290, 'FFFF800101', 5006, '7/1/1999', 336),
(1300, 'GGGG800101', 5005, '2/2/2003', 457),
(1310, 'HHHH800101', 5019, '3/8/2000', 463),
(1320, 'AAAA800101', 5018, '3/12/1999', 163),
(1330, 'BBBB800101', 5017, '12/6/1998', 558),
(1340, 'CCCC800101', 5016, '10/2/1999', 11),
(1350, 'DDDD800101', 5015, '5/6/1999', 330),
(1360, 'EEEE800101', 5014, '6/7/2001', 37),
(1370, 'FFFF800101', 5013, '5/6/2002', 423),
(1380, 'GGGG800101', 5012, '1/2/2001', 147),
(1390, 'HHHH800101', 5011, '6/1/2002', 308),
(1400, 'AAAA800101', 5010, '3/5/2002', 441),
(1410, 'BBBB800101', 5009, '5/11/2002', 461),
(1420, 'CCCC800101', 5008, '12/2/2001', 444),
(1430, 'DDDD800101', 5007, '10/6/2002', 506);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `entreganclave1000`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `entreganclave1000` (
`clave` int(4)
,`RFC` varchar(10)
,`numero` int(4)
,`fecha` varchar(10)
,`cantidad` int(3)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `fechasentrega`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `fechasentrega` (
`fecha` varchar(10)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materiales`
--

CREATE TABLE `materiales` (
  `Clave` decimal(5,0) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `Costo` decimal(8,2) DEFAULT NULL,
  `PorcentajeImpuesto` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `materiales`
--

INSERT INTO `materiales` (`Clave`, `Descripcion`, `Costo`, `PorcentajeImpuesto`) VALUES
('1000', 'Varilla 3/16', '100.00', '2.00'),
('1010', 'Varilla 4/32', '115.00', '2.02'),
('1020', 'Varilla 3/17', '130.00', '2.04'),
('1030', 'Varilla 4/33', '145.00', '2.06'),
('1040', 'Varilla 3/18', '160.00', '2.08'),
('1050', 'Varilla 4/34', '175.00', '2.10'),
('1060', 'Varilla 3/19', '190.00', '2.12'),
('1070', 'Varilla 4/35', '205.00', '2.14'),
('1080', 'Ladrillos rojos', '50.00', '2.16'),
('1090', 'Ladrillos grises', '35.00', '2.18'),
('1100', 'Block', '30.00', '2.20'),
('1110', 'Megablock', '40.00', '2.22'),
('1120', 'Sillar rosa', '100.00', '2.24'),
('1130', 'Sillar gris', '110.00', '2.26'),
('1140', 'Cantera blanca', '200.00', '2.28'),
('1150', 'Cantera gris', '1210.00', '2.30'),
('1160', 'Cantera rosa', '1420.00', '2.32'),
('1170', 'Cantera amarilla', '230.00', '2.34'),
('1180', 'Recubrimiento P1001', '200.00', '2.36'),
('1190', 'Recubrimiento P1010', '220.00', '2.38'),
('1200', 'Recubrimiento P1019', '240.00', '2.40'),
('1210', 'Recubrimiento P1028', '250.00', '2.42'),
('1220', 'Recubrimiento P1037', '280.00', '2.44'),
('1230', 'Cemento ', '300.00', '2.46'),
('1240', 'Arena', '200.00', '2.48'),
('1250', 'Grava', '100.00', '2.50'),
('1260', 'Gravilla', '90.00', '2.52'),
('1270', 'Tezontle', '80.00', '2.54'),
('1280', 'Tepetate', '34.00', '2.56'),
('1290', 'Tuber?a 3.5', '200.00', '2.58'),
('1300', 'Tuber?a 4.3', '210.00', '2.60'),
('1310', 'Tuber?a 3.6', '220.00', '2.62'),
('1320', 'Tuber?a 4.4', '230.00', '2.64'),
('1330', 'Tuber?a 3.7', '240.00', '2.66'),
('1340', 'Tuber?a 4.5', '250.00', '2.68'),
('1350', 'Tuber?a 3.8', '260.00', '2.70'),
('1360', 'Pintura C1010', '125.00', '2.72'),
('1370', 'Pintura B1020', '125.00', '2.74'),
('1380', 'Pintura C1011', '725.00', '2.76'),
('1390', 'Pintura B1021', '125.00', '2.78'),
('1400', 'Pintura C1011', '125.00', '2.80'),
('1410', 'Pintura B1021', '125.00', '2.82'),
('1420', 'Pintura C1012', '125.00', '2.84'),
('1430', 'Pintura B1022', '125.00', '2.86'),
('2000', 'grava', '20.00', '1.60'),
('2010', 'piso blanco', '5.00', '1.80'),
('2020', 'madera', '56.00', '3.00'),
('2030', 'cortinas blancas', '6.00', '4.60'),
('2040', 'mosquitero', '7.00', '2.00');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `materialesclave1060`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `materialesclave1060` (
`Clave` decimal(5,0)
,`Descripcion` varchar(50)
,`Costo` decimal(8,2)
,`PorcentajeImpuesto` decimal(6,2)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `RFC` char(13) NOT NULL,
  `RazonSocial` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`RFC`, `RazonSocial`) VALUES
('AAAA800101', 'La fragua'),
('BBBB800101', 'Oviedo'),
('CCCC800101', 'La Ferre'),
('DDDD800101', 'Cecoferre'),
('EEEE800101', 'Alvin'),
('FFFF800101', 'Comex'),
('GGGG800101', 'Tabiquera del centro'),
('HHHH800101', 'Tubasa');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `proveedorfragua`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `proveedorfragua` (
`razonsocial` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `Numero` decimal(5,0) NOT NULL,
  `Denominacion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proyectos`
--

INSERT INTO `proyectos` (`Numero`, `Denominacion`) VALUES
('5000', 'Vamos Mexico');

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `televisaproveedor`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `televisaproveedor` (
`denominacion` varchar(50)
,`RFC` char(13)
,`razonSocial` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `televisaproveedores`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `televisaproveedores` (
`denominacion` varchar(50)
,`RFC` char(13)
,`razonSocial` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `tipomaterial`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `tipomaterial` (
`descripcion` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `ventas`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `ventas` (
`clave` int(4)
,`venta` decimal(32,0)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `coahuilaproveedor`
--
DROP TABLE IF EXISTS `coahuilaproveedor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coahuilaproveedor`  AS  select `pv`.`RFC` AS `RFC` from ((`proyectos` `p` join `entregan` `e`) join `proveedores` `pv`) where `e`.`RFC` = `pv`.`RFC` and `e`.`numero` = `p`.`Numero` and `p`.`Denominacion` like 'Educando%' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `coahuilaproveedores`
--
DROP TABLE IF EXISTS `coahuilaproveedores`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `coahuilaproveedores`  AS  select `pv`.`RFC` AS `RFC` from ((`proyectos` `p` join `entregan` `e`) join `proveedores` `pv`) where `e`.`RFC` = `pv`.`RFC` and `e`.`numero` = `p`.`Numero` and `p`.`Denominacion` like 'Educando%' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `entreganclave1000`
--
DROP TABLE IF EXISTS `entreganclave1000`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `entreganclave1000`  AS  select `entregan`.`clave` AS `clave`,`entregan`.`RFC` AS `RFC`,`entregan`.`numero` AS `numero`,`entregan`.`fecha` AS `fecha`,`entregan`.`cantidad` AS `cantidad` from `entregan` where `entregan`.`clave` = 1000 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `fechasentrega`
--
DROP TABLE IF EXISTS `fechasentrega`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `fechasentrega`  AS  select distinct `e`.`fecha` AS `fecha` from `entregan` `e` where `e`.`fecha` >= '8/7/1998' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `materialesclave1060`
--
DROP TABLE IF EXISTS `materialesclave1060`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `materialesclave1060`  AS  select `materiales`.`Clave` AS `Clave`,`materiales`.`Descripcion` AS `Descripcion`,`materiales`.`Costo` AS `Costo`,`materiales`.`PorcentajeImpuesto` AS `PorcentajeImpuesto` from `materiales` where `materiales`.`Clave` = 1060 ;

-- --------------------------------------------------------

--
-- Estructura para la vista `proveedorfragua`
--
DROP TABLE IF EXISTS `proveedorfragua`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `proveedorfragua`  AS  select distinct `proveedores`.`RazonSocial` AS `razonsocial` from `proveedores` where `proveedores`.`RazonSocial` = 'La fragua' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `televisaproveedor`
--
DROP TABLE IF EXISTS `televisaproveedor`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `televisaproveedor`  AS  select `p`.`Denominacion` AS `denominacion`,`pv`.`RFC` AS `RFC`,`pv`.`RazonSocial` AS `razonSocial` from ((`proyectos` `p` join `entregan` `e`) join `proveedores` `pv`) where `e`.`RFC` = `pv`.`RFC` and `e`.`numero` = `p`.`Numero` and `p`.`Denominacion` like 'Televisa%' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `televisaproveedores`
--
DROP TABLE IF EXISTS `televisaproveedores`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `televisaproveedores`  AS  select `p`.`Denominacion` AS `denominacion`,`pv`.`RFC` AS `RFC`,`pv`.`RazonSocial` AS `razonSocial` from ((`proyectos` `p` join `entregan` `e`) join `proveedores` `pv`) where `e`.`RFC` = `pv`.`RFC` and `e`.`numero` = `p`.`Numero` and `p`.`Denominacion` like 'Televisa%' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `tipomaterial`
--
DROP TABLE IF EXISTS `tipomaterial`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tipomaterial`  AS  select distinct `materiales`.`Descripcion` AS `descripcion` from `materiales` where `materiales`.`Descripcion` = 'Varilla 3/16' ;

-- --------------------------------------------------------

--
-- Estructura para la vista `ventas`
--
DROP TABLE IF EXISTS `ventas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ventas`  AS  select `entregan`.`clave` AS `clave`,sum(`entregan`.`cantidad`) AS `venta` from `entregan` where `entregan`.`fecha` between '01/01/2000' and '31/12/2000' group by `entregan`.`clave` ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `materiales`
--
ALTER TABLE `materiales`
  ADD PRIMARY KEY (`Clave`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`RFC`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`Numero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
