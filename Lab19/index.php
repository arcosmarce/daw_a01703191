<!DOCTYPE html>
<html>
 <head>
 <meta charset="utf-8"/>
 <script type="text/javascript" src="js/script.js"></script>
 <link rel="stylesheet" href="css/bootstrap.min.css">
 <link rel="stylesheet" href="css/estilos.css">
 <script src="js/bootstrap.min.js"></script>
 <style>
 .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {

    padding-right: 2px;
    padding-left: 2px;
}
 </style>
 </head>
 <body>
 <h1>Lab 19 - AJAX</h1>
 <div class="container">
   <h1 class="main_title">Registrar alumnos</h1>
   <div class="content">
     <div class="panel panel-default">
       <div class="panel-body">
         <form>
           <div class="form-group">
             <div class="col-sm-2">
               <input type="text" id="nombres" class="form-control" placeholder="Nombres">
             </div>
             <div class="col-sm-2">
               <input type="text" id="apellidos" class="form-control" placeholder="Apellidos">
             </div>
             <div class="col-sm-2">
               <input type="text" id="email" class="form-control" placeholder="Email">
             </div>
             <div class="col-sm-2">
               <input type="text" id="telefono" class="form-control" placeholder="Telefono">
             </div>
             <div class="col-sm-1">
               <input type="button"  value="Registrar" onclick="add_member()">
             </div>
       
           </div>
         </form>
         <div style="clear:both"></div>
       </div>
     </div>
     <hr>
     <div class="panel panel-default">
       <div class="panel-body">
         <h3> Lista de alumnos</h3>
         <div id="list_container">
           <?php 
                        include('conexion.php');
                        $pdo = connect();
                        include('lista_alumnos.php'); 
                    ?>
         </div>
       </div>
       
   </div> 
 </div>


 <div>
    <h1 class="main_title">Preguntas Lab 19</h1>
    <h3>¿Qué importancia tiene AJAX en el desarrollo de RIA's (Rich Internet Applications?</h3>
    <p>Es de suma importancia ya que AJAX permite la comunicación asíncrona con el servidor en
     segundo plano  lo que significa que al momento de editar alguna parte de la aplicación no es necesaria la recarga de la página</p>
    <h3>¿Qué implicaciones de seguridad tiene AJAX? ¿Dónde se deben hacer las validaciones de seguridad, del lado del cliente o del lado del servidor?</h3>
    <p>Porque AJAX permite la comunicación asíncrona con el servidor en segundo plano y las validaciones de seguridad deben realizarse del lado del servidor porque se tiene más control sobre lo que suceda y sobre los datos</p>
    <h3>¿Qué es JSON?</h3>
    <p>La notación de objetos de javascript se refiere al intercambio ligero de los datos (interpretar y generar)</p>
    <br>
    <h3>REFERENCIAS</h3>
    <p >https://es.wikipedia.org/wiki/AJAX<p/>
    <p >https://academy.leewayweb.com/validaciones-lado-cliente-o-lado-servidor/<p/>
    <p >https://www.json.org/json-es.html/<p/>

 </div>
</body>
</html>