
function add_member() {
	var url = 'agregar_alumno.php';
	var method = 'POST';
	var params = 'nombres='+document.getElementById('nombres').value;
	params += '&apellidos='+document.getElementById('apellidos').value;
	params += '&email='+document.getElementById('email').value;
	params += '&telefono='+document.getElementById('telefono').value;
	var container_id = 'list_container' ;
	var loading_text = '<img src="images/fb_loading.gif">' ;
	
	ajax (url, method, params, container_id, loading_text) ;
}


function delete_member(id) {
	if (confirm('Confirma eliminar registro de alumno ?')) {
		var url = 'borrar_alumno.php';
		var method = 'POST';
		var params = 'id='+id;
		var container_id = 'list_container' ;
		ajax (url, method, params, container_id, loading_text) ;
	}
}

function ajax (url, method, params, container_id, loading_text) {
    try { 
    	xhr = new XMLHttpRequest();
    } catch(e) {
	    try{ 
	    	xhr = new ActiveXObject("Microsoft.XMLHTTP");
	    } catch(e1) { 
		    alert("Not supported!");
		}
	}
	xhr.onreadystatechange = function() {
						       if(xhr.readyState == 4) { 
							       document.getElementById(container_id).innerHTML = xhr.responseText;
							   } else {
							       document.getElementById(container_id).innerHTML = loading_text;
							   }
						   	}
	xhr.open(method, url, true);
	xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	xhr.send(params);
}