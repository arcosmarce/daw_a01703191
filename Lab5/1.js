

 function validarPassword () {
   
  var p1 = document.getElementById("password").value;
  var p2 = document.getElementById("password2").value;
  var espacios = false;
  var cont = 0;

	while (!espacios && (cont < p1.length)) {
		if (p1.charAt(cont) == " ")
			espacios = true;
		cont++;
	}
   
  if (espacios) {
   alert ("Ingrese una contraseña sin espacios en blanco");
   return false;
  }
   
  if (p1.length == 0 || p2.length == 0) {
   alert("Los campos de la password no pueden quedar vacios");
   return false;
  }
   
  if (p1 != p2) {
   alert("Las contraseñas deben coincidir");
   return false;
  } else {
   alert("Contraseña verificada");
   return true; 
  }
 }