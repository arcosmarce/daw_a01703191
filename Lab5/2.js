
        window.onload = function () {
            let productos = [
                {
                    id: 1,
                    nombre: 'Balón de soccer',
                    precio: 850,
                    imagen: 'soccer.jpg'
                },
                {
                    id: 2,
                    nombre: 'Balón de basquetbol',
                    precio: 380,
                    imagen: 'basquet.jpg'
                },
                {
                    id: 3,
                    nombre: 'Balón de voleybol',
                    precio: 590,
                    imagen: 'voleybol.png'
                }
            ]


            let $items = document.querySelector('#items');
            let carrito = [];
            let total = 0;
            let $carrito = document.querySelector('#carrito');
            let $total = document.querySelector('#total');
            

            function renderItems () {
                for (let info of productos) {
                    let nodo = document.createElement('div');
                    nodo.classList.add('card', 'col-sm-4');

                    let cardBody = document.createElement('div');
                    cardBody.classList.add('card-body');
        
                    let titulo = document.createElement('h5');
                    titulo.classList.add('card-title');
                    titulo.textContent = info['nombre'];
                    
                    let imagen = document.createElement('img');
                    imagen.classList.add('img-fluid');
                    imagen.setAttribute('src', info['imagen']);
                  
                    let precio = document.createElement('h1');
                    precio.classList.add('card-text');
                    precio.textContent = '$'+ info['precio'] ;
                    
                    let boton = document.createElement('button');
                    boton.classList.add('btn', 'btn-primary');
                    boton.textContent = '+';
                    boton.setAttribute('marcador', info['id']);
                    boton.addEventListener('click', addCarrito);
                  
                    cardBody.appendChild(imagen);
                    cardBody.appendChild(titulo);
                    cardBody.appendChild(precio);
                    cardBody.appendChild(boton);
                    nodo.appendChild(cardBody);
                    $items.appendChild(nodo);
                }
            }

            function addCarrito () {
                carrito.push(this.getAttribute('marcador'))
                calcularTotal();
                renderizarCarrito();
                calcularTotalIva();
            }

            function renderizarCarrito () {
                $carrito.textContent = '';
                let carritoOnly = [...new Set(carrito)];
                carritoOnly.forEach(function (item, indice) {
                    let miItem = productos.filter(function(itemBaseDatos) {
                        return itemBaseDatos['id'] == item;
                    });
                    let numeroUnidadesItem = carrito.reduce(function (total, itemId) {
                        return itemId === item ? total += 1 : total;
                    }, 0);

                    
                    let nodo = document.createElement('li');
                    nodo.classList.add('list-group-item', 'text-right', 'mx-2');
                    nodo.textContent = `${numeroUnidadesItem} - ${miItem[0]['nombre']} - $ ${miItem[0]['precio']} unidad`;
                 
                    let boton = document.createElement('button');
                    boton.classList.add('btn', 'btn-danger', 'mx-5');
                    boton.textContent = 'X';
                    boton.style.marginLeft = '1rem';
                    boton.setAttribute('item', item);
                    boton.addEventListener('click', borrarItemCarrito);
                    
                    nodo.appendChild(boton);
                    $carrito.appendChild(nodo);
                })
            }

            function borrarItemCarrito () {
                console.log()
                let id = this.getAttribute('item');
                carrito = carrito.filter(function (carritoId) {
                    return carritoId !== id;
                });

                renderizarCarrito();
                calcularTotal();
            }

            function calcularTotal () {
                total = 0;
                for (let item of carrito) {
                    let miItem = productos.filter(function(itemBaseDatos) {
                        return itemBaseDatos['id'] == item;
                    });
                    total = total + miItem[0]['precio'] + (miItem[0]['precio'])*0.10;
                }
                let totalDosDecimales = total.toFixed(0);
                $total.textContent = totalDosDecimales;
            }

            renderItems();

            function mensaje(){
                alert("Ha finalizado su compra, vuelva pronto");
            }
        } 
