-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-05-2020 a las 02:31:31
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `segundoparcial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidentes`
--

CREATE TABLE `incidentes` (
  `incidente_id` int(11) NOT NULL,
  `nombre` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `incidentes`
--

INSERT INTO `incidentes` (`incidente_id`, `nombre`) VALUES
(1, 'Falla eléctrica'),
(2, 'Fuga hervívoro'),
(3, 'Fuga velociraptors'),
(4, 'Fuga TRex'),
(5, 'Robo de ADN'),
(6, 'Auto descompuesto'),
(7, 'Visitantes en zona no autorizada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `incidentes_lugares`
--

CREATE TABLE `incidentes_lugares` (
  `lugar_id` int(11) NOT NULL,
  `incidente_id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `incidentes_lugares`
--

INSERT INTO `incidentes_lugares` (`lugar_id`, `incidente_id`, `fecha`) VALUES
(2, 1, '2020-05-06 16:28:55'),
(2, 4, '2020-05-03 06:00:00'),
(1, 3, '2020-05-04 06:00:00'),
(1, 3, '2020-05-04 06:00:00'),
(4, 3, '2020-05-04 06:00:00'),
(1, 2, '2020-05-04 06:00:00'),
(3, 2, '2020-05-04 06:00:00'),
(5, 5, '2020-05-04 06:00:00'),
(1, 7, '2020-05-04 06:00:00'),
(3, 0, '2020-05-06 23:04:01'),
(1, 0, '2020-05-06 23:05:04'),
(0, 1, '2020-05-06 23:05:04'),
(0, 2, '2020-05-06 23:09:31'),
(6, 0, '2020-05-06 23:10:29'),
(0, 3, '2020-05-06 23:10:29'),
(0, 3, '2020-05-06 23:13:11'),
(6, 0, '2020-05-06 23:31:01'),
(0, 3, '2020-05-06 23:31:01'),
(1, 0, '2020-05-06 23:31:56'),
(0, 1, '2020-05-06 23:31:56'),
(1, 0, '2020-05-06 23:32:46'),
(0, 3, '2020-05-06 23:32:46'),
(0, 2, '2020-05-06 23:34:49'),
(2, 0, '2020-05-06 23:36:23'),
(0, 1, '2020-05-06 23:36:23'),
(1, 0, '2020-05-06 23:38:35'),
(0, 0, '2020-05-06 23:39:46'),
(2, 0, '2020-05-06 23:42:06'),
(0, 2, '2020-05-06 23:42:06'),
(0, 0, '2020-05-07 00:04:54'),
(2, 0, '2020-05-07 00:05:00'),
(0, 0, '2020-05-07 00:18:03'),
(1, 3, '2020-05-07 00:18:28'),
(5, 5, '2020-05-07 00:18:52'),
(0, 0, '2020-05-07 00:20:48'),
(0, 0, '2020-05-07 00:20:49'),
(4, 2, '2020-05-07 00:20:54'),
(0, 0, '2020-05-07 00:22:55'),
(0, 0, '2020-05-07 00:27:54'),
(2, 4, '2020-05-07 00:28:04'),
(2, 2, '2020-05-07 00:29:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lugares`
--

CREATE TABLE `lugares` (
  `lugar_id` int(11) NOT NULL,
  `nombre` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lugares`
--

INSERT INTO `lugares` (`lugar_id`, `nombre`) VALUES
(1, 'Centro turistico'),
(2, 'Laboratorio'),
(3, 'Restaurante'),
(4, 'Centro operativo'),
(5, 'Triceratos'),
(6, 'Dilofosaurios'),
(7, 'Velociraptors'),
(8, 'TRex'),
(9, 'Planicie de los hervivoros');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
